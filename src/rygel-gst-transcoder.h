/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_GST_TRANSCODER_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_GST_TRANSCODER_H__

#include <glib.h>
#include <glib-object.h>
#include <rygel-server.h>
#include <gst/pbutils/encoding-profile.h>

G_BEGIN_DECLS

#define RYGEL_TYPE_GST_TRANSCODER (rygel_gst_transcoder_get_type ())
#define RYGEL_GST_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoder))
#define RYGEL_GST_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoderClass))
#define RYGEL_IS_GST_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_GST_TRANSCODER))
#define RYGEL_IS_GST_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_GST_TRANSCODER))
#define RYGEL_GST_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_GST_TRANSCODER, RygelGstTranscoderClass))

typedef struct _RygelGstTranscoder RygelGstTranscoder;
typedef struct _RygelGstTranscoderClass RygelGstTranscoderClass;
typedef struct _RygelGstTranscoderPrivate RygelGstTranscoderPrivate;

struct _RygelGstTranscoder {
  RygelTranscoder parent_instance;
  RygelGstTranscoderPrivate *priv;
};

struct _RygelGstTranscoderClass {
  RygelTranscoderClass parent_class;

  GstEncodingProfile * (* get_encoding_profile) (RygelGstTranscoder *self);
};

GType
rygel_gst_transcoder_get_type (void) G_GNUC_CONST;

RygelGstTranscoder *
rygel_gst_transcoder_new (void);

GstEncodingProfile *
rygel_gst_transcoder_get_encoding_profile (RygelGstTranscoder *self);

const gchar *
rygel_gst_transcoder_get_preset (RygelGstTranscoder *self);

void
rygel_gst_transcoder_set_preset (RygelGstTranscoder *self,
                                 const gchar        *value);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_GST_TRANSCODER_H__ */
