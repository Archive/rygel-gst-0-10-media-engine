/*
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Jens Georg <jensg@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <gee.h>
#include <rygel-core.h>
#include <gst/gst.h>
#include <libgupnp-dlna/gupnp-dlna.h>
#include "rygel-gst-data-source.h"
#include "rygel-gst-media-engine.h"
#include "rygel-aac-transcoder.h"
#include "rygel-avc-transcoder.h"
#include "rygel-l16-transcoder.h"
#include "rygel-mp2ts-transcoder.h"
#include "rygel-mp3-transcoder.h"
#include "rygel-wmv-transcoder.h"

G_DEFINE_TYPE (RygelGstMediaEngine, rygel_gst_media_engine, RYGEL_TYPE_MEDIA_ENGINE)

struct _RygelGstMediaEnginePrivate {
  GList *dlna_profiles;
  GList *transcoders;
};

#define RYGEL_GST_MEDIA_ENGINE_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), \
                                RYGEL_TYPE_GST_MEDIA_ENGINE, \
                                RygelGstMediaEnginePrivate))

RygelGstMediaEngine *
rygel_gst_media_engine_new (void) {
  return RYGEL_GST_MEDIA_ENGINE (g_object_new (RYGEL_TYPE_GST_MEDIA_ENGINE, NULL));
}

static GList *
rygel_gst_media_engine_real_get_dlna_profiles (RygelMediaEngine *base) {
  RygelGstMediaEngine *self = RYGEL_GST_MEDIA_ENGINE (base);

  return self->priv->dlna_profiles;
}

static GList *
rygel_gst_media_engine_real_get_transcoders (RygelMediaEngine *base) {
  RygelGstMediaEngine *self = RYGEL_GST_MEDIA_ENGINE (base);

  return self->priv->transcoders;
}


static RygelDataSource *
rygel_gst_media_engine_real_create_data_source (RygelMediaEngine *base G_GNUC_UNUSED,
                                                const gchar *uri) {
  RygelDataSource *result;
  GError *error;

  g_return_val_if_fail (uri != NULL, NULL);

  error = NULL;
  result = RYGEL_DATA_SOURCE (rygel_gst_data_source_new_from_uri (uri, &error));
  if (error) {
    g_warning ("rygel_gst_data_source_new() failed: %s", error->message);
    g_error_free (error);
  }

  return result;
}

static void
rygel_gst_media_engine_dispose (GObject *object) {
  RygelGstMediaEngine *self = RYGEL_GST_MEDIA_ENGINE (object);
  RygelGstMediaEnginePrivate *priv = self->priv;

  if (priv->dlna_profiles) {
    GList *profiles = priv->dlna_profiles;

    priv->dlna_profiles = NULL;
    g_list_free_full (profiles, (GDestroyNotify) rygel_dlna_profile_unref);
  }
  if (priv->transcoders) {
    GList *transcoders = priv->transcoders;

    priv->transcoders = NULL;
    g_list_free_full (transcoders, (GDestroyNotify) g_object_unref);
  }

  G_OBJECT_CLASS (rygel_gst_media_engine_parent_class)->dispose (object);
}

static void
rygel_gst_media_engine_class_init (RygelGstMediaEngineClass *gst_media_engine_class) {
  GObjectClass *object_class = G_OBJECT_CLASS (gst_media_engine_class);
  RygelMediaEngineClass *media_engine_class = RYGEL_MEDIA_ENGINE_CLASS (gst_media_engine_class);

  media_engine_class->get_dlna_profiles = rygel_gst_media_engine_real_get_dlna_profiles;
  media_engine_class->get_transcoders = rygel_gst_media_engine_real_get_transcoders;
  media_engine_class->create_data_source = rygel_gst_media_engine_real_create_data_source;
  object_class->dispose = rygel_gst_media_engine_dispose;

  g_type_class_add_private (gst_media_engine_class,
                            sizeof (RygelGstMediaEnginePrivate));
}

static void
rygel_gst_media_engine_init (RygelGstMediaEngine *self) {
  GUPnPDLNAProfileGuesser *guesser;
  const GList *profile_collection;
  GeeArrayList *transcoder_list = NULL;
  RygelConfiguration *config;
  GError *error = NULL;
  const GList *profile_it;

  self->priv = RYGEL_GST_MEDIA_ENGINE_GET_PRIVATE (self);

  gst_preset_set_app_dir (PRESET_DIR);

  /* Get the possible DLNA profiles
   * to add to the list of DLNA profiles supported by
   * this media engine, for get_dlna_profiles().
   * Note that these are DLNA profiles supported as sources,
   * not supported as transcoding profiles - the transcoders
   * report their own target DLNA profiles.
   */
  guesser = gupnp_dlna_profile_guesser_new (TRUE, FALSE);
  profile_collection = gupnp_dlna_profile_guesser_list_profiles (guesser);

  for (profile_it = profile_collection; profile_it != NULL; profile_it = profile_it->next) {
    GUPnPDLNAProfile *profile = GUPNP_DLNA_PROFILE (profile_it->data);
    const gchar *name = gupnp_dlna_profile_get_name (profile);
    const gchar *mime = gupnp_dlna_profile_get_mime (profile);

    /* TODO: Check that we (via GStreamer) really support this profile
     * instead of just claiming to support everything.
     */
    RygelDLNAProfile *rygel_profile = rygel_dlna_profile_new (name, mime);

    if (rygel_profile) {
      self->priv->dlna_profiles = g_list_prepend (self->priv->dlna_profiles, rygel_profile);
    }
  }

  self->priv->dlna_profiles = g_list_reverse (self->priv->dlna_profiles);

  /* Allow some transcoders to be disabled by the Rygel Server configuration.
   * For instance, some DLNA Renderers might incorrectly prefer inferior transcoded formats,
   * sometimes even preferring transcoded formats over the original data,
   * so this forces them to use other formats.
   */
  config = RYGEL_CONFIGURATION (rygel_meta_config_get_default ());

  rygel_configuration_get_transcoding (config, &error);
  if (error) {
    g_clear_error (&error);
  } else {
    /* Note that the default GStreamer media engine uses the same config group name. */
    transcoder_list = rygel_configuration_get_string_list (config,
                                                           "GstMediaEngine-0.10",
                                                           "transcoders",
                                                           &error);
    if (error) {
      g_clear_error (&error);
      transcoder_list = NULL;
    }
  }

  if (!transcoder_list) {
    transcoder_list = gee_array_list_new (G_TYPE_STRING, (GBoxedCopyFunc) g_strdup, g_free, NULL, NULL, NULL);

    /* Start with some defaults, at least for testing: */
    gee_abstract_collection_add (GEE_ABSTRACT_COLLECTION (transcoder_list), "lpcm");
    gee_abstract_collection_add (GEE_ABSTRACT_COLLECTION (transcoder_list), "mp3");
    gee_abstract_collection_add (GEE_ABSTRACT_COLLECTION (transcoder_list), "mp2ts");
    gee_abstract_collection_add (GEE_ABSTRACT_COLLECTION (transcoder_list), "wmv");
    gee_abstract_collection_add (GEE_ABSTRACT_COLLECTION (transcoder_list), "aac");
    gee_abstract_collection_add (GEE_ABSTRACT_COLLECTION (transcoder_list), "avc");
  }

  gint transcoder_size = gee_abstract_collection_get_size (GEE_ABSTRACT_COLLECTION (transcoder_list));
  gint transcoder_index = -1;
  while (TRUE) {
    gchar *transcoder_name;

    transcoder_index += 1;
    if (!(transcoder_index < transcoder_size)) {
      break;
    }

    transcoder_name = (gchar*) gee_abstract_list_get ((GeeAbstractList*)transcoder_list, transcoder_index);
    if (g_strcmp0 (transcoder_name, "lpcm") == 0) {
      RygelL16Transcoder *transcoder = rygel_l16_transcoder_new ();
      self->priv->transcoders = g_list_prepend (self->priv->transcoders, RYGEL_TRANSCODER (transcoder));
    } else if (g_strcmp0 (transcoder_name, "mp3") == 0) {
      RygelMP3Transcoder *transcoder = rygel_mp3_transcoder_new ();
      self->priv->transcoders = g_list_prepend (self->priv->transcoders, RYGEL_TRANSCODER (transcoder));
    } else if (g_strcmp0 (transcoder_name, "mp2ts") == 0) {
      RygelMP2TSTranscoder *transcoder = rygel_mp2_ts_transcoder_new_sd_eu ();
      self->priv->transcoders = g_list_prepend (self->priv->transcoders, RYGEL_TRANSCODER (transcoder));
      transcoder = rygel_mp2_ts_transcoder_new_hd_na ();
      self->priv->transcoders = g_list_prepend (self->priv->transcoders, RYGEL_TRANSCODER (transcoder));
    } else if (g_strcmp0 (transcoder_name, "wmv") == 0) {
      RygelWMVTranscoder *transcoder = rygel_wmv_transcoder_new ();
      self->priv->transcoders = g_list_prepend (self->priv->transcoders, RYGEL_TRANSCODER (transcoder));
    } else if (g_strcmp0 (transcoder_name, "aac") == 0) {
      RygelAACTranscoder *transcoder = rygel_aac_transcoder_new ();
      self->priv->transcoders = g_list_prepend (self->priv->transcoders, RYGEL_TRANSCODER (transcoder));
    } else if (g_strcmp0 (transcoder_name, "avc") == 0) {
      RygelAVCTranscoder *transcoder = rygel_avc_transcoder_new ();
      self->priv->transcoders = g_list_prepend (self->priv->transcoders, RYGEL_TRANSCODER (transcoder));
    } else {
      g_debug ("Unsupported transcoder: \"%s\"", transcoder_name);
    }

    g_free (transcoder_name);
  }

  self->priv->transcoders = g_list_reverse (self->priv->transcoders);

  g_object_unref (config);
  g_object_unref (transcoder_list);
  g_object_unref (guesser);
}

RygelMediaEngine*
module_get_instance (void) {
  gst_init (NULL, NULL);

  return RYGEL_MEDIA_ENGINE (rygel_gst_media_engine_new ());
}
