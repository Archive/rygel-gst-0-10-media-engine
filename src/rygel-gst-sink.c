/*
 * Copyright (C) 2011 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 *         Jens Georg <jensg@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-gst-sink.h"

G_DEFINE_TYPE (RygelGstSink, rygel_gst_sink, GST_TYPE_BASE_SINK)

enum
{
  RYGEL_GST_SINK_DUMMY_PROPERTY,
  RYGEL_GST_SINK_SOURCE,
  RYGEL_GST_SINK_OFFSETS
};

struct _RygelGstSinkPrivate {
  gint priority;
  gint64 chunks_buffered;
  gint64 bytes_sent;
  gint64 max_bytes;
  GMutex buffer_mutex;
  GCond buffer_condition;
  RygelDataSource *source;
  RygelHTTPSeek *offsets;
  gboolean frozen;
  GCancellable *cancellable;
};

typedef struct CallbackData_ CallbackData;

struct CallbackData_ {
  int ref_count;
  RygelGstSink *self;
  GstBuffer *buffer;
};

#define RYGEL_GST_SINK_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), \
                                RYGEL_TYPE_GST_SINK, \
                                RygelGstSinkPrivate))

#define RYGEL_GST_SINK_NAME "http-gst-sink"
#define RYGEL_GST_SINK_PAD_NAME "sink"
#define RYGEL_GST_SINK_MAX_BUFFERED_CHUNKS ((guint) 32)
#define RYGEL_GST_SINK_MIN_BUFFERED_CHUNKS ((guint) 4)

static void
rygel_gst_sink_on_cancelled (RygelGstSink *self) {
  RygelGstSinkPrivate *priv = self->priv;

  g_mutex_lock (&priv->buffer_mutex);
  g_cond_broadcast (&priv->buffer_condition);
  g_mutex_unlock (&priv->buffer_mutex);
}

static void
on_cancelled (GCancellable *sender G_GNUC_UNUSED,
              gpointer      user_data) {
  RygelGstSink *self = RYGEL_GST_SINK (user_data);

  rygel_gst_sink_on_cancelled (self);
}

RygelGstSink *
rygel_gst_sink_new (RygelDataSource *source,
                    RygelHTTPSeek   *offsets) {
  g_return_val_if_fail (RYGEL_IS_DATA_SOURCE (source), NULL);
  g_return_val_if_fail (RYGEL_IS_HTTP_SEEK (offsets) || offsets == NULL, NULL);

  return RYGEL_GST_SINK (g_object_new (RYGEL_TYPE_GST_SINK,
                                       "source", source,
                                       "offsets", offsets,
                                       NULL));
}

void
rygel_gst_sink_freeze (RygelGstSink *self) {
  g_return_if_fail (RYGEL_IS_GST_SINK (self));

  if (self->priv->frozen) {
    return;
  }

  g_mutex_lock (&self->priv->buffer_mutex);
  self->priv->frozen = TRUE;
  g_mutex_unlock (&self->priv->buffer_mutex);
}

void
rygel_gst_sink_thaw (RygelGstSink *self) {
  g_return_if_fail (RYGEL_IS_GST_SINK (self));

  if (!self->priv->frozen) {
    return;
  }

  g_mutex_lock (&self->priv->buffer_mutex);
  self->priv->frozen = FALSE;
  g_cond_broadcast (&self->priv->buffer_condition);
  g_mutex_unlock (&self->priv->buffer_mutex);
}

/* Runs in application thread */
static gboolean
rygel_gst_sink_push_data (RygelGstSink *self,
                          GstBuffer    *buffer) {

  gint64 left = self->priv->max_bytes - self->priv->bytes_sent;
  guint bufsize;
  gint64 to_send;


  if (g_cancellable_is_cancelled (self->priv->cancellable) ||
      left <= 0) {
    return FALSE;
  }

  bufsize = buffer->size;
  to_send = MIN ((gint64) bufsize, left);

  g_signal_emit_by_name (self->priv->source, "data-available", buffer->data, to_send);
  self->priv->chunks_buffered++;
  self->priv->bytes_sent += to_send;

  return FALSE;
}

static gboolean on_idle_push_data (gpointer user_data) {
  CallbackData *data = (CallbackData *) user_data;

  return rygel_gst_sink_push_data (data->self, data->buffer);
}

static void
callback_data_unref (gpointer user_data) {
  CallbackData *callback_data = (CallbackData *) user_data;

  if (g_atomic_int_dec_and_test (&callback_data->ref_count)) {
    RygelGstSink *self = callback_data->self;

    if (callback_data->buffer) {
      gst_buffer_unref (callback_data->buffer);
    }
    if (self) {
      gst_object_unref (self);
    }
    g_slice_free (CallbackData, callback_data);
  }
}

static GstFlowReturn
rygel_gst_sink_real_render (GstBaseSink *base,
                            GstBuffer   *buffer) {
  RygelGstSink *self = RYGEL_GST_SINK (base);
  CallbackData *callback_data;

  g_mutex_lock (&self->priv->buffer_mutex);
  while (!g_cancellable_is_cancelled (self->priv->cancellable) &&
         self->priv->frozen) {
    /* Client is either not reading (Paused) or not fast enough */
    g_cond_wait (&self->priv->buffer_condition, &self->priv->buffer_mutex);
  }
  g_mutex_unlock (&self->priv->buffer_mutex);

  if (g_cancellable_is_cancelled (self->priv->cancellable)) {
    return GST_FLOW_OK;
  }

  /* Use a ref-counted object to pass both this sink and the buffer
   * to the idle callback. */
  callback_data = g_slice_new0 (CallbackData);
  callback_data->ref_count = 1;
  callback_data->self = gst_object_ref (self);
  callback_data->buffer = gst_buffer_ref (buffer);
  g_idle_add_full (self->priv->priority,
                   on_idle_push_data,
                   callback_data,
                   callback_data_unref);

  return GST_FLOW_OK;
}

static void
clear_gmutex (GMutex *mutex) {
  GMutex zero_mutex = { 0 };
  if (memcmp (mutex, &zero_mutex, sizeof (GMutex))) {
    g_mutex_clear (mutex);
    memset (mutex, 0, sizeof (GMutex));
  }
}

static void
clear_gcond (GCond *mutex) {
  GCond zero_mutex = { 0 };
  if (memcmp (mutex, &zero_mutex, sizeof (GCond))) {
    g_cond_clear (mutex);
    memset (mutex, 0, sizeof (GCond));
  }
}

static void
rygel_gst_sink_dispose (GObject *object) {
  RygelGstSink *self = RYGEL_GST_SINK (object);
  RygelGstSinkPrivate *priv = self->priv;

  if (priv->cancellable) {
    GCancellable *cancellable = priv->cancellable;

    priv->cancellable = NULL;
    g_object_unref (cancellable);
  }
  if (priv->offsets) {
    RygelHTTPSeek *offsets = self->priv->offsets;

    self->priv->offsets = NULL;
    g_object_unref (offsets);
  }

  G_OBJECT_CLASS (rygel_gst_sink_parent_class)->dispose (object);
}

static void
rygel_gst_sink_finalize (GObject *object) {
  RygelGstSink *self = RYGEL_GST_SINK (object);
  RygelGstSinkPrivate *priv = self->priv;

  clear_gmutex (&priv->buffer_mutex);
  clear_gcond (&priv->buffer_condition);

  G_OBJECT_CLASS (rygel_gst_sink_parent_class)->finalize (object);
}

static void rygel_gst_sink_constructed (GObject *object)
{
  RygelGstSink *self = RYGEL_GST_SINK (object);
  RygelGstSinkPrivate *priv = self->priv;

  priv->chunks_buffered = 0;
  priv->bytes_sent = 0;
  priv->max_bytes = G_MAXINT64;
  priv->cancellable = g_cancellable_new ();

  gst_base_sink_set_sync (GST_BASE_SINK (self), FALSE);
  gst_object_set_name (GST_OBJECT (self), RYGEL_GST_SINK_NAME);
  priv->frozen = FALSE;

  if (priv->offsets &&
      rygel_http_seek_get_seek_type (priv->offsets) == RYGEL_HTTP_SEEK_TYPE_BYTE) {
    priv->max_bytes = rygel_http_seek_get_length (priv->offsets);
  }

  g_signal_connect_object (priv->cancellable,
                           "cancelled",
                           G_CALLBACK (on_cancelled),
                           self,
                           0);
}

static void
rygel_gst_sink_get_property (GObject    *object,
                             guint       property_id,
                             GValue     *value,
                             GParamSpec *pspec) {
  RygelGstSink *self = RYGEL_GST_SINK (object);
  RygelGstSinkPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_GST_SINK_SOURCE:
    g_value_set_object (value, priv->source);
    break;

  case RYGEL_GST_SINK_OFFSETS:
    g_value_set_object (value, priv->offsets);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_gst_sink_set_property (GObject      *object,
                             guint         property_id,
                             const GValue *value,
                             GParamSpec   *pspec) {
  RygelGstSink *self = RYGEL_GST_SINK (object);
  RygelGstSinkPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_GST_SINK_SOURCE:
    priv->source = g_value_get_object (value);
    break;

  case RYGEL_GST_SINK_OFFSETS:
    if (priv->offsets) {
      g_object_unref (priv->offsets);
    }
    priv->offsets = g_value_dup_object (value);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_gst_sink_class_init (RygelGstSinkClass *gst_sink_class) {
  GstBaseSinkClass *base_sink_class = GST_BASE_SINK_CLASS (gst_sink_class);
  GObjectClass *object_class = G_OBJECT_CLASS (gst_sink_class);
  GstElementClass *element_class = GST_ELEMENT_CLASS (gst_sink_class);
  GstCaps* caps;
  GstPadTemplate* template;

  base_sink_class->render = rygel_gst_sink_real_render;
  object_class->dispose = rygel_gst_sink_dispose;
  object_class->finalize = rygel_gst_sink_finalize;
  object_class->constructed = rygel_gst_sink_constructed;
  object_class->get_property = rygel_gst_sink_get_property;
  object_class->set_property = rygel_gst_sink_set_property;

  g_object_class_install_property (object_class,
                                   RYGEL_GST_SINK_SOURCE,
                                   g_param_spec_object ("source",
                                                        "source",
                                                        "source",
                                                        RYGEL_TYPE_DATA_SOURCE,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_CONSTRUCT_ONLY |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   RYGEL_GST_SINK_OFFSETS,
                                   g_param_spec_object ("offsets",
                                                        "offsets",
                                                        "offsets",
                                                        RYGEL_TYPE_HTTP_SEEK,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_CONSTRUCT_ONLY |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));

  caps = gst_caps_new_any ();
  template =  gst_pad_template_new (RYGEL_GST_SINK_PAD_NAME,
                                    GST_PAD_SINK,
                                    GST_PAD_ALWAYS,
                                    caps);
  gst_element_class_add_pad_template (element_class, template);
  gst_object_unref (template);

  g_type_class_add_private (gst_sink_class, sizeof (RygelGstSinkPrivate));
}

static void
rygel_gst_sink_init (RygelGstSink *self) {
  self->priv = RYGEL_GST_SINK_GET_PRIVATE (self);

  g_mutex_init (&self->priv->buffer_mutex);
  g_cond_init (&self->priv->buffer_condition);
}

GCancellable *
rygel_gst_sink_get_cancellable (RygelGstSink *sink)
{
  g_return_val_if_fail (RYGEL_IS_GST_SINK (sink), NULL);

  return sink->priv->cancellable;
}
