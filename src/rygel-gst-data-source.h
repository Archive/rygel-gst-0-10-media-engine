/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_GST_DATA_SOURCE_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_GST_DATA_SOURCE_H__

#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>

G_BEGIN_DECLS

#define RYGEL_TYPE_GST_DATA_SOURCE (rygel_gst_data_source_get_type ())
#define RYGEL_GST_DATA_SOURCE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_GST_DATA_SOURCE, RygelGstDataSource))
#define RYGEL_GST_DATA_SOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_GST_DATA_SOURCE, RygelGstDataSourceClass))
#define RYGEL_IS_GST_DATA_SOURCE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_GST_DATA_SOURCE))
#define RYGEL_IS_GST_DATA_SOURCE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_GST_DATA_SOURCE))
#define RYGEL_GST_DATA_SOURCE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_GST_DATA_SOURCE, RygelGstDataSourceClass))

typedef struct _RygelGstDataSource RygelGstDataSource;
typedef struct _RygelGstDataSourceClass RygelGstDataSourceClass;
typedef struct _RygelGstDataSourcePrivate RygelGstDataSourcePrivate;

struct _RygelGstDataSource {
  GObject parent_instance;
  RygelGstDataSourcePrivate *priv;
};

struct _RygelGstDataSourceClass {
  GObjectClass parent_class;
};

GType
rygel_gst_data_source_get_type (void) G_GNUC_CONST;

RygelGstDataSource *
rygel_gst_data_source_new_from_uri (const gchar  *uri,
                                    GError      **error);

RygelGstDataSource *
rygel_gst_data_source_new (GstElement *source);

GstElement *
rygel_gst_data_source_get_gst_element (RygelGstDataSource *self);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_GST_DATA_SOURCE_H__ */
