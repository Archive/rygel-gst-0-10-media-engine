/*
 * Copyright (C) 2009 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 * Author: Murray Cumming <murrayc@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-mp3-transcoder.h"

/**
 * Transcoder for mpeg 1 layer 3 audio.
 */

G_DEFINE_TYPE (RygelMP3Transcoder,
	       rygel_mp3_transcoder,
	       RYGEL_TYPE_AUDIO_TRANSCODER)

#define RYGEL_MP3_TRANSCODER_BITRATE 128
#define RYGEL_MP3_TRANSCODER_FORMAT "audio/mpeg,mpegversion=1,layer=3"

RygelMP3Transcoder*
rygel_mp3_transcoder_new (void) {
  return RYGEL_MP3_TRANSCODER (g_object_new (RYGEL_TYPE_MP3_TRANSCODER,
                                             "mime-type", "audio/mpeg",
                                             "dlna-profile", "MP3",
                                             "extension", "mp3",
                                             /* No "preset". */
                                             "audio-bitrate", RYGEL_MP3_TRANSCODER_BITRATE,
                                             /* No "container-caps". */
                                             "audio-caps", RYGEL_MP3_TRANSCODER_FORMAT,
                                             NULL));
}

static void
rygel_mp3_transcoder_class_init (RygelMP3TranscoderClass *mp3_transcoder_class G_GNUC_UNUSED) {
}

static void
rygel_mp3_transcoder_init (RygelMP3Transcoder *self G_GNUC_UNUSED) {
}
