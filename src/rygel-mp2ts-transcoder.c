/*
 * Copyright (C) 2009 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 * Author: Murray Cumming <murrayc@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-mp2ts-transcoder.h"

/**
 * Transcoder for mpeg transport stream containing mpeg 2 video and mp2 audio.
 */

G_DEFINE_TYPE (RygelMP2TSTranscoder,
               rygel_mp2_ts_transcoder,
               RYGEL_TYPE_VIDEO_TRANSCODER)

enum
{
  RYGEL_MP2_TS_TRANSCODER_DUMMY_PROPERTY,
  RYGEL_MP2_TS_TRANSCODER_PROFILE
};

GType
rygel_mp2_ts_profile_get_type (void)
{
  static volatile gsize g_define_type_id__volatile = 0;

  if (g_once_init_enter (&g_define_type_id__volatile))
    {
      static const GEnumValue values[] = {
        { RYGEL_MP2_TS_PROFILE_SD, "RYGEL_MP2_TS_PROFILE_SD", "sd" },
        { RYGEL_MP2_TS_PROFILE_HD, "RYGEL_MP2_TS_PROFILE_HD", "hd" },
        { 0, NULL, NULL }
      };
      GType g_define_type_id =
        g_enum_register_static (g_intern_static_string ("RygelMP2TSProfile"), values);
      g_once_init_leave (&g_define_type_id__volatile, g_define_type_id);
    }

  return g_define_type_id__volatile;
}

struct _RygelMP2TSTranscoderPrivate {
  RygelMP2TSProfile profile;
};

#define RYGEL_MP2_TS_TRANSCODER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), \
                                RYGEL_TYPE_MP2_TS_TRANSCODER, \
                                RygelMP2TSTranscoderPrivate))

#define RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE 1500
#define RYGEL_MP2_TS_TRANSCODER_AUDIO_BITRATE 192
#define RYGEL_MP2_TS_TRANSCODER_CONTAINER "video/mpegts,systemstream=true,packetsize=188"
#define RYGEL_MP2_TS_TRANSCODER_AUDIO_FORMAT "audio/mpeg,mpegversion=1,layer=2"
#define RYGEL_MP2_TS_TRANSCODER_BASE_VIDEO_FORMAT "video/mpeg,mpegversion=2,systemstream=false"
#define RYGEL_MP2_TS_TRANSCODER_RESTRICTION_TEMPLATE "video/x-raw-yuv,framerate=(fraction)%d/1,width=%d,height=%d"
#define RYGEL_MP2_TS_TRANSCODER_WIDTH_SD_EU 720
#define RYGEL_MP2_TS_TRANSCODER_WIDTH_HD_NA 1280
#define RYGEL_MP2_TS_TRANSCODER_HEIGHT_SD_EU 576
#define RYGEL_MP2_TS_TRANSCODER_HEIGHT_HD_NA 720
#define RYGEL_MP2_TS_TRANSCODER_FRAME_RATE_SD_EU 25
#define RYGEL_MP2_TS_TRANSCODER_FRAME_RATE_HD_NA 30
#define RYGEL_MP2_TS_TRANSCODER_PROFILE_SD_EU "MPEG_TS_SD_EU_ISO"
#define RYGEL_MP2_TS_TRANSCODER_PROFILE_HD_NA "MPEG_TS_HD_NA_ISO"
#define RYGEL_MP2_TS_TRANSCODER_RESTRICTION(fr, w, h) "video/x-raw-yuv" \
                                                      ",framerate=(fraction)" G_STRINGIFY (fr) "/1" \
                                                      ",width=" G_STRINGIFY (w) \
                                                      ",height=" G_STRINGIFY (h)
#define RYGEL_MP2_TS_TRANSCODER_RESTRICTION_SD_EU \
  RYGEL_MP2_TS_TRANSCODER_RESTRICTION (RYGEL_MP2_TS_TRANSCODER_FRAME_RATE_SD_EU, \
				       RYGEL_MP2_TS_TRANSCODER_WIDTH_SD_EU, \
				       RYGEL_MP2_TS_TRANSCODER_HEIGHT_SD_EU)
#define RYGEL_MP2_TS_TRANSCODER_RESTRICTION_HD_NA \
  RYGEL_MP2_TS_TRANSCODER_RESTRICTION (RYGEL_MP2_TS_TRANSCODER_FRAME_RATE_HD_NA, \
				       RYGEL_MP2_TS_TRANSCODER_WIDTH_HD_NA, \
				       RYGEL_MP2_TS_TRANSCODER_HEIGHT_HD_NA)

static const gint RYGEL_MP2_TS_TRANSCODER_WIDTH[2] = {
  RYGEL_MP2_TS_TRANSCODER_WIDTH_SD_EU,
  RYGEL_MP2_TS_TRANSCODER_WIDTH_HD_NA
};
static const gint RYGEL_MP2_TS_TRANSCODER_HEIGHT[2] = {
  RYGEL_MP2_TS_TRANSCODER_HEIGHT_SD_EU,
  RYGEL_MP2_TS_TRANSCODER_HEIGHT_HD_NA
};
static const gint RYGEL_MP2_TS_TRANSCODER_FRAME_RATE[2] = {
  RYGEL_MP2_TS_TRANSCODER_FRAME_RATE_SD_EU,
  RYGEL_MP2_TS_TRANSCODER_FRAME_RATE_HD_NA
};

RygelMP2TSTranscoder *
rygel_mp2_ts_transcoder_new_sd_eu (void)
{
  return RYGEL_MP2_TS_TRANSCODER (g_object_new (RYGEL_TYPE_MP2_TS_TRANSCODER,
                                                "mime-type", "video/mpeg",
                                                "dlna-profile", RYGEL_MP2_TS_TRANSCODER_PROFILE_SD_EU,
                                                "extension", "mpg",
                                                /* No "preset". */
                                                "audio-bitrate", RYGEL_MP2_TS_TRANSCODER_AUDIO_BITRATE,
                                                "container-caps", RYGEL_MP2_TS_TRANSCODER_CONTAINER,
                                                "audio-caps", RYGEL_MP2_TS_TRANSCODER_AUDIO_FORMAT,
                                                "video-bitrate", RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE,
                                                "video-caps", RYGEL_MP2_TS_TRANSCODER_BASE_VIDEO_FORMAT,
                                                "video-restrictions", RYGEL_MP2_TS_TRANSCODER_RESTRICTION_SD_EU,
                                                "mp2-profile", RYGEL_MP2_TS_PROFILE_SD,
                                                NULL));
}

RygelMP2TSTranscoder *
rygel_mp2_ts_transcoder_new_hd_na (void)
{
  return RYGEL_MP2_TS_TRANSCODER (g_object_new (RYGEL_TYPE_MP2_TS_TRANSCODER,
                                                "mime-type", "video/mpeg",
                                                "dlna-profile", RYGEL_MP2_TS_TRANSCODER_PROFILE_HD_NA,
                                                "extension", "mpg",
                                                /* No "preset". */
                                                "audio-bitrate", RYGEL_MP2_TS_TRANSCODER_AUDIO_BITRATE,
                                                "container-caps", RYGEL_MP2_TS_TRANSCODER_CONTAINER,
                                                "audio-caps", RYGEL_MP2_TS_TRANSCODER_AUDIO_FORMAT,
                                                "video-bitrate", RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE,
                                                "video-caps", RYGEL_MP2_TS_TRANSCODER_BASE_VIDEO_FORMAT,
                                                "video-restrictions", RYGEL_MP2_TS_TRANSCODER_RESTRICTION_HD_NA,
                                                "mp2-profile", RYGEL_MP2_TS_PROFILE_HD,
                                                NULL));
}

static GUPnPDIDLLiteResource *
rygel_mp2_ts_transcoder_real_add_resource (RygelTranscoder        *base,
                                           GUPnPDIDLLiteItem      *didl_item,
                                           RygelMediaItem         *item,
                                           RygelTranscodeManager  *manager,
                                           GError                **error) {
  RygelMP2TSTranscoder *self = RYGEL_MP2_TS_TRANSCODER (base);
  GError *inner_error = NULL;
  GUPnPDIDLLiteResource* resource;
  gint bitrate;

  g_return_val_if_fail (GUPNP_IS_DIDL_LITE_ITEM (didl_item), NULL);
  g_return_val_if_fail (RYGEL_IS_MEDIA_ITEM (item), NULL);
  g_return_val_if_fail (RYGEL_IS_TRANSCODE_MANAGER (manager), NULL);

  resource = RYGEL_TRANSCODER_CLASS (rygel_mp2_ts_transcoder_parent_class)->add_resource (RYGEL_TRANSCODER (self), didl_item, item, manager, &inner_error);
  if (inner_error) {
    g_propagate_error (error, inner_error);
    return NULL;
  }

  if (!resource) {
    return NULL;
  }

  bitrate = ((RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE + RYGEL_MP2_TS_TRANSCODER_AUDIO_BITRATE) * 1000) / 8;
  gupnp_didl_lite_resource_set_width (resource,
                                      RYGEL_MP2_TS_TRANSCODER_WIDTH[self->priv->profile]);
  gupnp_didl_lite_resource_set_height (resource,
                                       RYGEL_MP2_TS_TRANSCODER_HEIGHT[self->priv->profile]);
  gupnp_didl_lite_resource_set_bitrate (resource, bitrate);

  return resource;
}

static guint
rygel_mp2_ts_transcoder_real_get_distance (RygelTranscoder *base,
                                           RygelMediaItem  *item) {
  RygelMP2TSTranscoder *self = RYGEL_MP2_TS_TRANSCODER (base);
  RygelVideoItem *video_item;
  guint distance;
  guint bitrate;
  guint width;
  guint height;

  g_return_val_if_fail (RYGEL_IS_MEDIA_ITEM (item), G_MAXUINT);

  if (!RYGEL_IS_VIDEO_ITEM (item)) {
    return G_MAXUINT;
  }

  video_item = RYGEL_VIDEO_ITEM (item);
  distance = 0;

  bitrate = rygel_audio_item_get_bits_per_sample (RYGEL_AUDIO_ITEM (video_item));
  if (bitrate > 0) {
    distance += abs(bitrate - RYGEL_MP2_TS_TRANSCODER_VIDEO_BITRATE);
  }

  width = rygel_visual_item_get_width (RYGEL_VISUAL_ITEM (video_item));
  if (width > 0) {
    distance += abs(width - RYGEL_MP2_TS_TRANSCODER_WIDTH[self->priv->profile]);
  }

  height = rygel_visual_item_get_height (RYGEL_VISUAL_ITEM (video_item));
  if (height > 0) {
    distance += abs(height - RYGEL_MP2_TS_TRANSCODER_HEIGHT[self->priv->profile]);
  }

  return distance;
}

static void
rygel_mp2_ts_transcoder_get_property (GObject    *object,
                                      guint       property_id,
                                      GValue     *value,
                                      GParamSpec *pspec) {
  RygelMP2TSTranscoder *self = RYGEL_MP2_TS_TRANSCODER (object);
  RygelMP2TSTranscoderPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_MP2_TS_TRANSCODER_PROFILE:
    g_value_set_enum (value, priv->profile);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_mp2_ts_transcoder_set_property (GObject      *object,
                                      guint         property_id,
                                      const GValue *value,
                                      GParamSpec   *pspec) {
  RygelMP2TSTranscoder *self = RYGEL_MP2_TS_TRANSCODER (object);
  RygelMP2TSTranscoderPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_MP2_TS_TRANSCODER_PROFILE:
    priv->profile = g_value_get_enum (value);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_mp2_ts_transcoder_class_init (RygelMP2TSTranscoderClass *mp2_ts_transcoder_class) {
  GObjectClass *object_class = G_OBJECT_CLASS (mp2_ts_transcoder_class);
  RygelTranscoderClass *transcoder_class = RYGEL_TRANSCODER_CLASS (mp2_ts_transcoder_class);

  transcoder_class->add_resource = rygel_mp2_ts_transcoder_real_add_resource;
  transcoder_class->get_distance = rygel_mp2_ts_transcoder_real_get_distance;
  object_class->get_property = rygel_mp2_ts_transcoder_get_property;
  object_class->set_property = rygel_mp2_ts_transcoder_set_property;

  g_object_class_install_property (object_class,
                                   RYGEL_MP2_TS_TRANSCODER_PROFILE,
                                   g_param_spec_enum ("mp2-profile",
                                                      "mp2-profile",
                                                      "mp2-profile",
                                                      RYGEL_TYPE_MP2_TS_PROFILE,
                                                      RYGEL_MP2_TS_PROFILE_SD,
                                                      G_PARAM_STATIC_NAME |
                                                      G_PARAM_STATIC_NICK |
                                                      G_PARAM_STATIC_BLURB |
                                                      G_PARAM_READABLE |
                                                      G_PARAM_WRITABLE));

  g_type_class_add_private (mp2_ts_transcoder_class,
                            sizeof (RygelMP2TSTranscoderPrivate));
}

static void
rygel_mp2_ts_transcoder_init (RygelMP2TSTranscoder *self) {
  self->priv = RYGEL_MP2_TS_TRANSCODER_GET_PRIVATE (self);
}
