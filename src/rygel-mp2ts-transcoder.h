/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_MP2TS_TRANSCODER_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_MP2TS_TRANSCODER_H__

#include <glib.h>
#include <glib-object.h>
#include "rygel-video-transcoder.h"

G_BEGIN_DECLS

#define RYGEL_TYPE_MP2_TS_TRANSCODER (rygel_mp2_ts_transcoder_get_type ())
#define RYGEL_MP2_TS_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_MP2_TS_TRANSCODER, RygelMP2TSTranscoder))
#define RYGEL_MP2_TS_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_MP2_TS_TRANSCODER, RygelMP2TSTranscoderClass))
#define RYGEL_IS_MP2_TS_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_MP2_TS_TRANSCODER))
#define RYGEL_IS_MP2_TS_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_MP_2TS_TRANSCODER))
#define RYGEL_MP2_TS_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_MP2_TS_TRANSCODER, RygelMP2TSTranscoderClass))

typedef struct _RygelMP2TSTranscoder RygelMP2TSTranscoder;
typedef struct _RygelMP2TSTranscoderClass RygelMP2TSTranscoderClass;
typedef struct _RygelMP2TSTranscoderPrivate RygelMP2TSTranscoderPrivate;

struct _RygelMP2TSTranscoder {
  RygelVideoTranscoder parent_instance;
  RygelMP2TSTranscoderPrivate *priv;
};

struct _RygelMP2TSTranscoderClass {
  RygelVideoTranscoderClass parent_class;
};

typedef enum  {
  RYGEL_MP2_TS_PROFILE_SD = 0,
  RYGEL_MP2_TS_PROFILE_HD
} RygelMP2TSProfile;

#define RYGEL_TYPE_MP2_TS_PROFILE (rygel_mp2_ts_profile_get_type ())

GType
rygel_mp2_ts_profile_get_type (void) G_GNUC_CONST;

GType
rygel_mp2_ts_transcoder_get_type (void) G_GNUC_CONST;

RygelMP2TSTranscoder *
rygel_mp2_ts_transcoder_new_sd_eu (void);

RygelMP2TSTranscoder *
rygel_mp2_ts_transcoder_new_hd_na (void);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_MP2TS_TRANSCODER_H__ */
