/*
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Jens Georg <jensg@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <glib/gi18n-lib.h>
#include <rygel-server.h>
#include "rygel-gst-data-source.h"
#include "rygel-gst-errors.h"
#include "rygel-gst-sink.h"
#include "rygel-gst-utils.h"

static void
rygel_gst_data_source_rygel_data_source_interface_init (RygelDataSourceIface *iface);

G_DEFINE_TYPE_WITH_CODE (RygelGstDataSource,
                         rygel_gst_data_source,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RYGEL_TYPE_DATA_SOURCE,
                                                rygel_gst_data_source_rygel_data_source_interface_init))

#define RYGEL_GST_DATA_SOURCE_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), \
                                RYGEL_TYPE_GST_DATA_SOURCE, \
                                RygelGstDataSourcePrivate))
#define RYGEL_GST_SINK_NAME "http-gst-sink"

enum  {
  RYGEL_GST_DATA_SOURCE_DUMMY_PROPERTY,
  RYGEL_GST_DATA_SOURCE_SOURCE
};

struct _RygelGstDataSourcePrivate {
  GstElement *src;
  GstPipeline *pipeline;
  RygelHTTPSeek *seek;
  RygelGstSink *sink;
  guint bus_watch_id;
};

RygelGstDataSource *
rygel_gst_data_source_new_from_uri (const gchar  *uri,
                                    GError      **error) {
  GstElement *source = rygel_gst_utils_create_source_for_uri (uri);
  RygelGstDataSource *data_source;

  if (source == NULL) {
    g_set_error (error,
                 RYGEL_GST_DATA_SOURCE_ERROR,
                 RYGEL_GST_DATA_SOURCE_ERROR_NOT_COMPATIBLE,
                 "Could not create GstElement for URI %s",
                 uri);

    return NULL;
  }

  data_source = rygel_gst_data_source_new (source);
  gst_object_unref (source);

  return data_source;
}

RygelGstDataSource *
rygel_gst_data_source_new (GstElement *source) {
  g_return_val_if_fail (GST_IS_ELEMENT (source), NULL);

  return RYGEL_GST_DATA_SOURCE (g_object_new (RYGEL_TYPE_GST_DATA_SOURCE,
                                              "source", source,
                                              NULL));
}

static void
on_pad_added (GstElement *src G_GNUC_UNUSED,
              GstPad     *src_pad,
              gpointer    user_data) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (user_data);
  GstCaps *caps = gst_pad_get_caps_reffed (src_pad);
  GstElement *sink = gst_bin_get_by_name (GST_BIN (self->priv->pipeline),
                                          RYGEL_GST_SINK_NAME);
  GstPad *sink_pad = NULL;
  GstElement *depay = rygel_gst_utils_get_rtp_depayloader (caps);

  if (depay) {
    gst_bin_add (GST_BIN (self->priv->pipeline), depay);

    if (!gst_element_link (depay, sink)) {
       gchar *depay_name = gst_object_get_name (GST_OBJECT (depay));
       gchar *sink_name = gst_object_get_name (GST_OBJECT (sink));

       g_critical (_("Failed to link %s to %s"),
		   depay_name,
		   sink_name);
       g_free (depay_name);
       g_free (sink_name);

       g_signal_emit_by_name (self, "done");
       goto done;
    }

    sink_pad = gst_element_get_compatible_pad (depay, src_pad, caps);
  } else {
    sink_pad = gst_element_get_compatible_pad (sink, src_pad, caps);
  }


  if (gst_pad_link (src_pad, sink_pad) != GST_PAD_LINK_OK) {
    gchar *src_pad_name = gst_object_get_name (GST_OBJECT (src_pad));
    gchar *sink_pad_name = gst_object_get_name (GST_OBJECT (sink_pad));

    g_critical (_("Failed to link pad %s to %s"),
		src_pad_name,
		sink_pad_name);
    g_free (src_pad_name);
    g_free (sink_pad_name);

    g_signal_emit_by_name (self, "done");
    goto done;
  }

  if (depay) {
    gst_element_sync_state_with_parent (depay);
  }

done:
  if (depay) {
    gst_object_unref (depay);
  }
  if (sink_pad) {
    gst_object_unref (sink_pad);
  }
  if (sink) {
    gst_object_unref (sink);
  }
  if (caps) {
    gst_caps_unref (caps);
  }
}

static gboolean
rygel_gst_data_source_perform_seek (RygelGstDataSource *self) {
  GstSeekType stop_type = GST_SEEK_TYPE_NONE;
  GstFormat format;
  GstSeekFlags flags = GST_SEEK_FLAG_FLUSH;
  gint64 start;
  gint64 stop;

  if (self->priv->seek &&
      rygel_http_seek_get_length (self->priv->seek) >= rygel_http_seek_get_total_length (self->priv->seek)) {
    return TRUE;
  }

  if (rygel_http_seek_get_seek_type (self->priv->seek) == RYGEL_HTTP_SEEK_TYPE_TIME) {
    format = GST_FORMAT_TIME;
    flags |= GST_SEEK_FLAG_KEY_UNIT;
    start = rygel_http_seek_get_start (self->priv->seek) * GST_USECOND;
    stop = rygel_http_seek_get_stop (self->priv->seek) * GST_USECOND;
  } else {
    format = GST_FORMAT_BYTES;
    flags |= GST_SEEK_FLAG_ACCURATE;
    start = rygel_http_seek_get_start (self->priv->seek);
    stop = rygel_http_seek_get_stop (self->priv->seek);
  }

  if (rygel_http_seek_get_stop (self->priv->seek) > 0) {
    stop_type = GST_SEEK_TYPE_SET;
  }

  if (!gst_element_seek (GST_ELEMENT (self->priv->pipeline),
                         1.0,
                         format,
                         flags,
                         GST_SEEK_TYPE_SET,
                         start,
                         stop_type,
                         stop + 1)) {
    GError *error;

    g_warning(_("Failed to seek to offsets %" G_GINT64_FORMAT ":%" G_GINT64_FORMAT),
      rygel_http_seek_get_start (self->priv->seek),
      rygel_http_seek_get_stop (self->priv->seek));

    error  = g_error_new_literal (RYGEL_DATA_SOURCE_ERROR,
                                  RYGEL_DATA_SOURCE_ERROR_SEEK_FAILED,
                                  _("Failed to seek"));
    g_signal_emit_by_name (self, "error", error);
    g_error_free (error);

    return FALSE;
  }

  return TRUE;
}

static gboolean
on_idle_emit_done (gpointer user_data) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (user_data);

  g_signal_emit_by_name (self, "done");
  return FALSE;
}

static gboolean
on_bus_watch (GstBus     *bus G_GNUC_UNUSED,
              GstMessage *message,
              gpointer    user_data) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (user_data);
  gboolean ret = TRUE;

  if (message->type == GST_MESSAGE_EOS) {
    ret = FALSE;
  } else if (message->type == GST_MESSAGE_STATE_CHANGED) {
    GstState old_state;
    GstState new_state;

    if (message->src != GST_OBJECT (self->priv->pipeline)) {
      return TRUE;
    }

    gst_message_parse_state_changed (message, &old_state, &new_state, NULL);

    if (old_state == GST_STATE_NULL && new_state == GST_STATE_READY) {
      GstElement* element = gst_bin_get_by_name (GST_BIN (self->priv->pipeline), "muxer");
      if (element) {
        GstElementFactory* factory = gst_element_get_factory (element);
        if (factory) {
          const gchar *name = gst_plugin_feature_get_name (GST_PLUGIN_FEATURE (factory));

          // Awesome gross hack, really.
          if (g_strcmp0 (name, "mp4mux") == 0) {
            g_object_set (element, "streamable", TRUE, NULL);
            g_object_set (element, "fragment-duration", 1000, NULL);
          }

          gst_object_unref (factory);
        }

         gst_object_unref (element);
      }
    }

    if (self->priv->seek &&
        old_state == GST_STATE_READY &&
        new_state == GST_STATE_PAUSED &&
        rygel_gst_data_source_perform_seek (self)) {
      gst_element_set_state (GST_ELEMENT (self->priv->pipeline),
                             GST_STATE_PLAYING);
    }
  } else {
    gchar *err_msg = NULL;
    gchar *pipeline_name = gst_object_get_name (GST_OBJECT (self->priv->pipeline));
    GError *error = NULL;

    if (message->type == GST_MESSAGE_ERROR) {
      gst_message_parse_error (message, &error, &err_msg);
      g_critical (_("Error from pipeline %s: %s %s"),
                  pipeline_name,
                  error->message,
                  err_msg);

      ret = FALSE;
    } else if (message->type == GST_MESSAGE_WARNING) {
      gst_message_parse_warning (message, &error, &err_msg);
      g_warning (_("Warning from pipeline %s: %s %s"),
                 pipeline_name,
                 error->message,
                 err_msg);
    }

    if (error != NULL) {
      g_error_free (error);
    }
    g_free (pipeline_name);
    g_free (err_msg);
  }

  if (!ret) {
    g_idle_add_full (G_PRIORITY_DEFAULT,
                     on_idle_emit_done,
                     g_object_ref (self),
                     g_object_unref);
  }

  return ret;
}

static void
rygel_gst_data_source_prepare_pipeline (RygelGstDataSource  *self,
                                        const gchar         *name,
                                        GstElement          *src,
                                        GError             **error) {
  GstBus *bus;

  self->priv->sink = rygel_gst_sink_new (RYGEL_DATA_SOURCE (self), self->priv->seek);
  gst_object_ref_sink (self->priv->sink);

  self->priv->pipeline = GST_PIPELINE (gst_pipeline_new (name));
  if (self->priv->pipeline == NULL) {
    g_set_error_literal (error,
                         RYGEL_DATA_SOURCE_ERROR,
                         RYGEL_DATA_SOURCE_ERROR_GENERAL,
                         _("Failed to create pipeline"));
    return;
  }

  gst_bin_add_many (GST_BIN (self->priv->pipeline),
                    self->priv->src,
                    self->priv->sink,
                    NULL);

  if (src->numsrcpads == 0) {
    // Seems source uses dynamic pads, so link when the pad is available

    g_signal_connect_object (self->priv->src,
                             "pad-added",
                             G_CALLBACK (on_pad_added),
                             self,
                             0);
  } else {
    // static pads? easy!
    if (!gst_element_link (self->priv->src, GST_ELEMENT (self->priv->sink))) {
      gchar *src_name = gst_object_get_name (GST_OBJECT (self->priv->src));
      gchar *sink_name = gst_object_get_name (GST_OBJECT (self->priv->sink));

      g_set_error (error,
                   RYGEL_GST_ERROR,
                   RYGEL_GST_ERROR_LINK,
                   "Failed to link %s to %s",
                   src_name,
                   sink_name);
      g_free (src_name);
      g_free (sink_name);

      return;
    }
  }

  // Bus handler
  bus = gst_pipeline_get_bus (self->priv->pipeline);
  self->priv->bus_watch_id = gst_bus_add_watch_full (bus,
                                                     G_PRIORITY_DEFAULT,
                                                     on_bus_watch,
                                                     g_object_ref (self),
                                                     g_object_unref);
  gst_object_unref (bus);
}

static void
rygel_gst_data_source_real_start (RygelDataSource  *base,
                                  RygelHTTPSeek    *offsets,
                                  GError          **error) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (base);
  GError *inner_error = NULL;

  if (offsets) {
    g_object_ref (offsets);
  }
  if (self->priv->seek) {
    g_object_unref (self->priv->seek);
  }
  self->priv->seek = offsets;
  rygel_gst_data_source_prepare_pipeline (self, "RygelGstDataSource", self->priv->src, &inner_error);
  if (inner_error) {
    g_propagate_error (error, inner_error);
    return;
  }

  if (self->priv->seek) {
    gst_element_set_state (GST_ELEMENT (self->priv->pipeline), GST_STATE_PAUSED);
  } else {
    gst_element_set_state (GST_ELEMENT (self->priv->pipeline), GST_STATE_PLAYING);
  }
}

static void
rygel_gst_data_source_real_freeze (RygelDataSource *base) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (base);

  rygel_gst_sink_freeze (self->priv->sink);
}

static void
rygel_gst_data_source_real_thaw (RygelDataSource *base) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (base);

  rygel_gst_sink_thaw (self->priv->sink);
}

static void
rygel_gst_data_source_real_stop (RygelDataSource *base) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (base);

  if (self->priv->sink) {
    g_cancellable_cancel (rygel_gst_sink_get_cancellable (self->priv->sink));
  }

  if (self->priv->pipeline) {
    gst_element_set_state (GST_ELEMENT (self->priv->pipeline), GST_STATE_NULL);
  }

  g_source_remove (self->priv->bus_watch_id);

  g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                   on_idle_emit_done,
                   g_object_ref (self),
                   g_object_unref);
}

static void
rygel_gst_data_source_dispose (GObject *object) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (object);
  RygelGstDataSourcePrivate *priv = self->priv;

  if (priv->sink) {
    g_cancellable_cancel (rygel_gst_sink_get_cancellable (priv->sink));
    gst_object_unref (priv->sink);
    priv->sink = NULL;
  }

  if (priv->pipeline) {
    gst_element_set_state (GST_ELEMENT (priv->pipeline), GST_STATE_NULL);
    gst_object_unref (priv->pipeline);
    priv->pipeline = NULL;
  }

  if (priv->src) {
    gst_object_unref (priv->src);
  }
  if (priv->seek) {
    g_object_unref (priv->seek);
  }

  G_OBJECT_CLASS (rygel_gst_data_source_parent_class)->dispose (object);
}

static void
rygel_gst_data_source_get_property (GObject *object, guint property_id, GValue *value, GParamSpec *pspec) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (object);
  RygelGstDataSourcePrivate *priv = self->priv;

  switch (property_id) {
    case RYGEL_GST_DATA_SOURCE_SOURCE:
      g_value_set_object (value, priv->src);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
rygel_gst_data_source_set_property (GObject *object, guint property_id, const GValue *value, GParamSpec *pspec) {
  RygelGstDataSource *self = RYGEL_GST_DATA_SOURCE (object);
  RygelGstDataSourcePrivate *priv = self->priv;

  switch (property_id) {
    case RYGEL_GST_DATA_SOURCE_SOURCE:
      if (priv->src) {
        g_object_unref (priv->src);
      }
      priv->src = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

static void
rygel_gst_data_source_class_init (RygelGstDataSourceClass *gst_data_source_class) {
  GObjectClass *object_class = G_OBJECT_CLASS (gst_data_source_class);

  object_class->dispose = rygel_gst_data_source_dispose;
  object_class->get_property = rygel_gst_data_source_get_property;
  object_class->set_property = rygel_gst_data_source_set_property;
  g_object_class_install_property (object_class,
                                   RYGEL_GST_DATA_SOURCE_SOURCE,
                                   g_param_spec_object ("source",
                                                        "source",
                                                        "source",
                                                        GST_TYPE_ELEMENT,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_CONSTRUCT_ONLY |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));
  g_type_class_add_private (gst_data_source_class, sizeof (RygelGstDataSourcePrivate));
}

static void
rygel_gst_data_source_rygel_data_source_interface_init (RygelDataSourceIface *iface) {
  iface->start = rygel_gst_data_source_real_start;
  iface->freeze = rygel_gst_data_source_real_freeze;
  iface->thaw = rygel_gst_data_source_real_thaw;
  iface->stop = rygel_gst_data_source_real_stop;
}

static void
rygel_gst_data_source_init (RygelGstDataSource *self) {
  self->priv = RYGEL_GST_DATA_SOURCE_GET_PRIVATE (self);
}

/**
 * rygel_gst_data_source_get_gst_element:
 * @self: A #RygelGstDataSource
 *
 * Retrieves the underyling GStreamer element, for use by
 *
 * Return value: a #GstElement
 */
GstElement*
rygel_gst_data_source_get_gst_element (RygelGstDataSource *self)
{
  g_return_val_if_fail (RYGEL_IS_GST_DATA_SOURCE (self), NULL);

  return self->priv->src;
}
