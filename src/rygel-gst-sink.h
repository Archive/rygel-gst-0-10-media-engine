/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_GST_SINK_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_GST_SINK_H__

#include <glib.h>
#include <glib-object.h>
#include <gio/gio.h>
#include <rygel-server.h>
#include <gst/base/gstbasesink.h>

G_BEGIN_DECLS

#define RYGEL_TYPE_GST_SINK (rygel_gst_sink_get_type ())
#define RYGEL_GST_SINK(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_GST_SINK, RygelGstSink))
#define RYGEL_GST_SINK_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_GST_SINK, RygelGstSinkClass))
#define RYGEL_IS_GST_SINK(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_GST_SINK))
#define RYGEL_IS_GST_SINK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_GST_SINK))
#define RYGEL_GST_SINK_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_GST_SINK, RygelGstSinkClass))

typedef struct _RygelGstSink RygelGstSink;
typedef struct _RygelGstSinkClass RygelGstSinkClass;
typedef struct _RygelGstSinkPrivate RygelGstSinkPrivate;

struct _RygelGstSink {
  GstBaseSink parent_instance;
  RygelGstSinkPrivate *priv;
};

struct _RygelGstSinkClass {
  GstBaseSinkClass parent_class;
};

GType
rygel_gst_sink_get_type (void) G_GNUC_CONST;

RygelGstSink *
rygel_gst_sink_new (RygelDataSource *source,
                    RygelHTTPSeek   *offsets);

void
rygel_gst_sink_freeze (RygelGstSink *self);

void
rygel_gst_sink_thaw (RygelGstSink *self);

GCancellable *
rygel_gst_sink_get_cancellable (RygelGstSink *sink);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_GST_SINK_H__ */
