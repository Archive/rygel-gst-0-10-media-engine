/*
 * Copyright (C) 2011 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Luis de Bethencourt <luis.debethencourt@collabora.com>
 * Author: Murray Cumming <murrayc@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-avc-transcoder.h"

G_DEFINE_TYPE (RygelAVCTranscoder, rygel_avc_transcoder, RYGEL_TYPE_VIDEO_TRANSCODER)

#define RYGEL_AVC_TRANSCODER_VIDEO_BITRATE 1200
#define RYGEL_AVC_TRANSCODER_AUDIO_BITRATE 64
#define RYGEL_AVC_TRANSCODER_CONTAINER "video/quicktime,variant=iso"
#define RYGEL_AVC_TRANSCODER_AUDIO_CAPS "audio/mpeg,mpegversion=4"
#define RYGEL_AVC_TRANSCODER_VIDEO_CAPS "video/x-h264,stream-format=avc"
#define RYGEL_AVC_TRANSCODER_RESTRICTIONS "video/x-raw-yuv,framerate=(fraction)15/1,width=352,height=288"

RygelAVCTranscoder *
rygel_avc_transcoder_new (void) {
  return RYGEL_AVC_TRANSCODER (g_object_new (RYGEL_TYPE_AVC_TRANSCODER,
                                             "mime-type", "video/mp4",
                                             "dlna-profile", "AVC_MP4_BL_CIF15_AAC_520",
                                             "extension", "mp4",
                                             "preset", "Rygel AVC_MP4_BL_CIF15_AAC_520 preset",
                                             "audio-bitrate", RYGEL_AVC_TRANSCODER_AUDIO_BITRATE,
                                             "container-caps", RYGEL_AVC_TRANSCODER_CONTAINER,
                                             "audio-caps", RYGEL_AVC_TRANSCODER_AUDIO_CAPS,
                                             "video-bitrate", RYGEL_AVC_TRANSCODER_VIDEO_BITRATE,
                                             "video-caps", RYGEL_AVC_TRANSCODER_VIDEO_CAPS,
                                             "video-restrictions", RYGEL_AVC_TRANSCODER_RESTRICTIONS,
                                             NULL));
}

static GUPnPDIDLLiteResource *
rygel_avc_transcoder_real_add_resource (RygelTranscoder        *base,
                                        GUPnPDIDLLiteItem      *didl_item,
                                        RygelMediaItem         *item,
                                        RygelTranscodeManager  *manager,
                                        GError                **error) {
  GUPnPDIDLLiteResource *resource;
  GError *inner_error = NULL;

  g_return_val_if_fail (GUPNP_IS_DIDL_LITE_ITEM (didl_item), NULL);
  g_return_val_if_fail (RYGEL_IS_MEDIA_ITEM (item), NULL);
  g_return_val_if_fail (RYGEL_IS_TRANSCODE_MANAGER (manager), NULL);

  resource = RYGEL_TRANSCODER_CLASS (rygel_avc_transcoder_parent_class)->add_resource (base, didl_item, item, manager, &inner_error);
  if (inner_error != NULL) {
    g_propagate_error (error, inner_error);
    return NULL;
  }

  if (!resource) {
    return NULL;
  }

  gupnp_didl_lite_resource_set_width (resource, 352);
  gupnp_didl_lite_resource_set_height (resource, 288);

  return resource;
}

static void
rygel_avc_transcoder_class_init (RygelAVCTranscoderClass *avc_transcoder_class) {
  RygelTranscoderClass *transcoder_class = RYGEL_TRANSCODER_CLASS (avc_transcoder_class);

  transcoder_class->add_resource = rygel_avc_transcoder_real_add_resource;
}

static void
rygel_avc_transcoder_init (RygelAVCTranscoder *self G_GNUC_UNUSED) {
}
