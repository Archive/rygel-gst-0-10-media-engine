/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_GST_UTILS_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_GST_UTILS_H__

#include <glib.h>
#include <gst/gst.h>
#include <gst/pbutils/encoding-profile.h>

G_BEGIN_DECLS

GstElement*
rygel_gst_utils_create_element (const gchar  *factory_name,
                                const gchar  *name,
                                GError      **error);

GstElement*
rygel_gst_utils_create_source_for_uri (const gchar *uri);

void
rygel_gst_utils_dump_encoding_profile (GstEncodingProfile *profile,
                                       gint                indent);

GstElement *
rygel_gst_utils_get_rtp_depayloader (GstCaps *caps);

GstCaps *
rygel_gst_utils_caps_from_gvalue (const GValue *value);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_GST_UTILS_H__ */
