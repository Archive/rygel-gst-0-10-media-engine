/*
 * Copyright (C) 2009 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Zeeshan Ali (Khattak) <zeeshanak@gnome.org>
 *                               <zeeshan.ali@nokia.com>
 * Author: Murray Cumming <murrayc@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <glib/gi18n-lib.h>
#include "rygel-gst-errors.h"
#include "rygel-gst-utils.h"

GstElement *
rygel_gst_utils_create_element (const gchar  *factory_name,
                                const gchar  *name,
                                GError      **error) {
  GstElement *element;

  g_return_val_if_fail (factory_name != NULL, NULL);

  element = gst_element_factory_make (factory_name, name);
  if (element) {
    gst_object_ref_sink (element);
  } else {
    g_set_error (error,
                 RYGEL_GST_ERROR,
                 RYGEL_GST_ERROR_MISSING_PLUGIN,
                 _("Required element %s missing"),
                 factory_name);
  }

  return element;
}

GstElement *
rygel_gst_utils_create_source_for_uri (const gchar *uri) {
  GstElement *src;

  g_return_val_if_fail (uri != NULL, NULL);

  src = gst_element_make_from_uri (GST_URI_SRC, uri, NULL);

  if (src) {
    if (GST_OBJECT_IS_FLOATING (GST_OBJECT (src))) {
      gst_object_ref_sink (src);
    }

    if (g_object_class_find_property (G_OBJECT_GET_CLASS (src), "blocksize")) {
      g_object_set (src, "blocksize", (glong) 65536, NULL);
    }

    if (g_object_class_find_property (G_OBJECT_GET_CLASS (src), "tcp-timeout")) {
      g_object_set (src, "tcp-timeout", (gint64) 60000000, NULL);
    }
  }

  return src;
}

void
rygel_gst_utils_dump_encoding_profile (GstEncodingProfile *profile,
                                       gint                indent) {
  gchar *indent_s;
  const GstCaps *caps;
  gchar *format_name;
  const GstCaps *restriction;

  g_return_if_fail (profile != NULL);

  indent_s = g_strnfill ((gsize) indent, ' ');
  g_debug ("%s%s:", indent_s, gst_encoding_profile_get_name (profile));

  caps = gst_encoding_profile_get_format (profile);
  format_name = gst_caps_to_string (caps);
  g_debug ("%s  Format: %s", indent_s, format_name);
  g_free (format_name);

  restriction = gst_encoding_profile_get_restriction (profile);
  if (restriction) {
    gchar *restriction_name = gst_caps_to_string (restriction);
    g_debug ("%s  Restriction: %s", indent_s, restriction_name);
    g_free (restriction_name);
  }

  if (GST_IS_ENCODING_CONTAINER_PROFILE (profile)) {
    GstEncodingContainerProfile *container = GST_ENCODING_CONTAINER_PROFILE (profile);
    const GList *subprofile_collection = gst_encoding_container_profile_get_profiles (container);
    const GList *subprofile_it;

    for (subprofile_it = subprofile_collection; subprofile_it != NULL; subprofile_it = subprofile_it->next) {
      GstEncodingProfile *subprofile = GST_ENCODING_PROFILE (subprofile_it->data);

      rygel_gst_utils_dump_encoding_profile (subprofile, indent + 4);
    }
  }

  g_free (indent_s);
}

static gboolean
rygel_gst_utils_need_rtp_depayloader (GstCaps *caps) {
  const GstStructure *structure;
  const gchar *name;

  g_return_val_if_fail (caps != NULL, FALSE);

  structure = gst_caps_get_structure (caps, (guint) 0);
  name = gst_structure_get_name (structure);

  return (g_strcmp0 (name, "application/x-rtp") == 0);
}

GstElement *
rygel_gst_utils_get_rtp_depayloader (GstCaps *caps) {
  GList *features;
  GList *filtered;
  const gchar *feature_name;

  if (!rygel_gst_utils_need_rtp_depayloader (caps)) {
    return NULL;
  }

  features = gst_element_factory_list_get_elements ((GstElementFactoryListType) GST_ELEMENT_FACTORY_TYPE_DEPAYLOADER, GST_RANK_NONE);
  filtered = gst_element_factory_list_filter (features, caps, GST_PAD_SINK, FALSE);
  gst_plugin_feature_list_free (features);

  feature_name = gst_plugin_feature_get_name (GST_PLUGIN_FEATURE (filtered->data));
  /* If most "fitting" depayloader was rtpdepay skip it because it is
   * just some kind of proxy.
   */
  if (g_strcmp0 (feature_name, "rtpdepay") == 0) {
    if (filtered->next) {
      GstElement* element = gst_element_factory_create (GST_ELEMENT_FACTORY (filtered->next->data), NULL);
      if (element) {
        gst_object_ref_sink (element);
      }

      gst_plugin_feature_list_free (filtered);
      return element;
    }

    return NULL;
  } else {
    GstElement* element = gst_element_factory_create (GST_ELEMENT_FACTORY (filtered->data), NULL);
    if (element) {
      gst_object_ref_sink (element);
    }

    gst_plugin_feature_list_free (filtered);
    return element;
  }
}

/**
 * rygel_gst_utils_caps_from_gvalue:
 * Convert a #GValue holding a string to #GstCaps or return %NULL if the
 * #GValue holds a %NULL string or no string at all.
 **/
GstCaps *
rygel_gst_utils_caps_from_gvalue (const GValue *value)
{
  const gchar *str;

  if (!G_VALUE_HOLDS (value, G_TYPE_STRING)) {
    return NULL;
  }

  str = g_value_get_string (value);
  if (str == NULL) {
    return NULL;
  }

  return gst_caps_from_string (str);
}
