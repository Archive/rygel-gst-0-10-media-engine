/*
 * Copyright (C) 2009 Jens Georg <mail@jensge.org>.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Jens Georg <mail@jensge.org>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-wmv-transcoder.h"

G_DEFINE_TYPE (RygelWMVTranscoder,
               rygel_wmv_transcoder,
               RYGEL_TYPE_VIDEO_TRANSCODER)

#define RYGEL_WMV_TRANSCODER_VIDEO_BITRATE 1200
#define RYGEL_WMV_TRANSCODER_AUDIO_BITRATE 64

RygelWMVTranscoder *
rygel_wmv_transcoder_new (void) {
  return RYGEL_WMV_TRANSCODER (g_object_new (RYGEL_TYPE_WMV_TRANSCODER,
                                             "mime-type", "video/x-ms-wmv",
                                             "dlna-profile", "WMVHIGH_FULL",
                                             "extension", "wmv",
                                             /* No "preset". */
                                             "audio-bitrate", RYGEL_WMV_TRANSCODER_AUDIO_BITRATE,
                                             "container-caps", "video/x-ms-asf,parsed=true",
                                             "audio-caps", "audio/x-wma,channels=2,wmaversion=1",
                                             "video-bitrate", RYGEL_WMV_TRANSCODER_VIDEO_BITRATE,
                                             "video-caps", "video/x-wmv,wmvversion=1",
                                             /* No "video-restrictions". */
                                             NULL));
}

static void
rygel_wmv_transcoder_class_init (RygelWMVTranscoderClass *wmv_transcoder_class G_GNUC_UNUSED) {
}

static void
rygel_wmv_transcoder_init (RygelWMVTranscoder *self G_GNUC_UNUSED) {
}
