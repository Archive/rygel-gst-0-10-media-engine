/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_AAC_TRANSCODER_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_AAC_TRANSCODER_H__


#include <glib.h>
#include <glib-object.h>
#include "rygel-audio-transcoder.h"


G_BEGIN_DECLS

#define RYGEL_TYPE_AAC_TRANSCODER (rygel_aac_transcoder_get_type ())
#define RYGEL_AAC_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_AAC_TRANSCODER, RygelAACTranscoder))
#define RYGEL_AAC_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_AAC_TRANSCODER, RygelAACTranscoderClass))
#define RYGEL_IS_AAC_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_AAC_TRANSCODER))
#define RYGEL_IS_AAC_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_AAC_TRANSCODER))
#define RYGEL_AAC_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_AAC_TRANSCODER, RygelAACTranscoderClass))

typedef struct _RygelAACTranscoder RygelAACTranscoder;
typedef struct _RygelAACTranscoderClass RygelAACTranscoderClass;
typedef struct _RygelAACTranscoderPrivate RygelAACTranscoderPrivate;

struct _RygelAACTranscoder {
  RygelAudioTranscoder parent_instance;
  RygelAACTranscoderPrivate *priv;
};

struct _RygelAACTranscoderClass {
  RygelAudioTranscoderClass parent_class;
};

GType
rygel_aac_transcoder_get_type (void) G_GNUC_CONST;

RygelAACTranscoder *
rygel_aac_transcoder_new (void);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_AAC_TRANSCODER_H__ */
