/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_VIDEO_TRANSCODER_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_VIDEO_TRANSCODER_H__

#include <glib.h>
#include <glib-object.h>
#include "rygel-audio-transcoder.h"

G_BEGIN_DECLS

#define RYGEL_TYPE_VIDEO_TRANSCODER (rygel_video_transcoder_get_type ())
#define RYGEL_VIDEO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoder))
#define RYGEL_VIDEO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoderClass))
#define RYGEL_IS_VIDEO_TRANSCODER(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_VIDEO_TRANSCODER))
#define RYGEL_IS_VIDEO_TRANSCODER_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_VIDEO_TRANSCODER))
#define RYGEL_VIDEO_TRANSCODER_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_VIDEO_TRANSCODER, RygelVideoTranscoderClass))

typedef struct _RygelVideoTranscoder RygelVideoTranscoder;
typedef struct _RygelVideoTranscoderClass RygelVideoTranscoderClass;
typedef struct _RygelVideoTranscoderPrivate RygelVideoTranscoderPrivate;


struct _RygelVideoTranscoder {
  RygelAudioTranscoder parent_instance;
  RygelVideoTranscoderPrivate *priv;
};

struct _RygelVideoTranscoderClass {
  RygelAudioTranscoderClass parent_class;
};

GType
rygel_video_transcoder_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_VIDEO_TRANSCODER_H__ */
