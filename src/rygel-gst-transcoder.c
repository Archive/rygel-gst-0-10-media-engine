/*
 * Copyright (C) 2009-2012 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-gst-data-source.h"
#include "rygel-gst-errors.h"
#include "rygel-gst-transcoder.h"
#include "rygel-gst-utils.h"

#define RYGEL_GST_TRANSCODER_DEFAULT_ENCODING_PRESET "Rygel DLNA preset"
#define RYGEL_GST_TRANSCODER_DECODE_BIN "decodebin2"
#define RYGEL_GST_TRANSCODER_ENCODE_BIN "encodebin"
#define RYGEL_GST_TRANSCODER_DESCRIPTION "Encoder and decoder are not compatible"

enum  {
  RYGEL_GST_TRANSCODER_DUMMY_PROPERTY,
  RYGEL_GST_TRANSCODER_PRESET
};

G_DEFINE_TYPE (RygelGstTranscoder, rygel_gst_transcoder, RYGEL_TYPE_TRANSCODER)

struct _RygelGstTranscoderPrivate {
  gchar *preset;
  GstElement *decoder;
  GstElement *encoder;
  gboolean link_failed;
};

#define RYGEL_GST_TRANSCODER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), \
                                RYGEL_TYPE_GST_TRANSCODER, \
                                RygelGstTranscoderPrivate))

static void
on_decoder_pad_added (GstElement *decodebin G_GNUC_UNUSED,
                      GstPad     *new_pad,
                      gpointer    user_data) {
  RygelGstTranscoder* self = RYGEL_GST_TRANSCODER (user_data);
  GstPad* sinkpad = gst_element_get_compatible_pad (self->priv->encoder,
                                                    new_pad,
                                                    NULL);

  if (!sinkpad) {
    GstCaps *caps =  gst_pad_get_caps_reffed (new_pad);

    g_signal_emit_by_name (self->priv->encoder, "request-pad", caps, &sinkpad, NULL);
    gst_caps_unref (caps);
  }

  if (!sinkpad) {
    g_debug ("No compatible encodebin pad found for pad '%s'. Ignoring.",
             gst_object_get_name (GST_OBJECT (new_pad)));
    return;
  }

  if (gst_pad_link (new_pad, sinkpad) == GST_PAD_LINK_OK) {
    self->priv->link_failed = FALSE;
  } else {
    g_warning ("Failed to link pad '%s' to '%s'",
               gst_object_get_name (GST_OBJECT (new_pad)),
               gst_object_get_name (GST_OBJECT (sinkpad)));
  }

  gst_object_unref (sinkpad);
}

static gboolean
on_decoder_autoplug_continue (GstElement *decodebin G_GNUC_UNUSED,
                              GstPad     *new_pad G_GNUC_UNUSED,
                              GstCaps    *caps,
                              gpointer    user_data) {
  RygelGstTranscoder* self = RYGEL_GST_TRANSCODER (user_data);
  GstPad* sinkpad = NULL;

  g_signal_emit_by_name (self->priv->encoder, "request-pad", caps, &sinkpad, NULL);
  if (!sinkpad) {
    return TRUE;
  }

  gst_object_unref (sinkpad);
  return FALSE;
}

static void
on_decoder_no_more_pads (GstElement* decodebin G_GNUC_UNUSED,
                         gpointer user_data) {
  RygelGstTranscoder* self = RYGEL_GST_TRANSCODER (user_data);
  RygelGstTranscoderPrivate *priv = self->priv;

  /* We haven't found any pads we could link */
  if (priv->link_failed) {
    /* Signal that error. */
    GstBin *bin = GST_BIN (gst_object_get_parent (GST_OBJECT (priv->encoder)));
    GError *error = g_error_new_literal (G_IO_ERROR,
                                         G_IO_ERROR_FAILED,
                                         "Could not link");
    GstMessage* message = gst_message_new_error (GST_OBJECT (bin),
                                                 error,
                                                 RYGEL_GST_TRANSCODER_DESCRIPTION);
    GstBus* bus = gst_element_get_bus (GST_ELEMENT (bin));

    gst_bus_post (bus, message);

    gst_object_unref (bus);
    g_error_free (error);
    gst_object_unref (bin);
  }
}

static RygelDataSource *
rygel_gst_transcoder_create_source (RygelTranscoder  *base,
                                    RygelMediaItem   *item,
                                    RygelDataSource  *src,
                                    GError          **error) {
  RygelGstTranscoder *self;
  RygelGstTranscoderPrivate *priv;
  RygelGstDataSource *data_source;
  GstEncodingProfile *encoding_profile;
  GstBin *bin;
  GError *inner_error = NULL;
  GstElement *element;
  GstPad *pad;
  GstGhostPad *ghost_pad;
  RygelDataSource *result;

  g_return_val_if_fail (RYGEL_IS_MEDIA_ITEM (item), NULL);
  /* We can only link GStreamer data sources together,
   * so check that the RygelDataSource is of the expected type:
   */
  g_return_val_if_fail (RYGEL_IS_GST_DATA_SOURCE (src), NULL);

  self = RYGEL_GST_TRANSCODER (base);
  priv = self->priv;
  data_source = RYGEL_GST_DATA_SOURCE (src);

  if (priv->decoder) {
    g_object_unref (priv->decoder);
  }

  priv->decoder = rygel_gst_utils_create_element (RYGEL_GST_TRANSCODER_DECODE_BIN,
                                                  RYGEL_GST_TRANSCODER_DECODE_BIN,
                                                  &inner_error);
  if (inner_error) {
    g_propagate_error (error, inner_error);
    return NULL;
  }

  if (priv->encoder) {
    g_object_unref (priv->encoder);
  }

  priv->encoder = rygel_gst_utils_create_element (RYGEL_GST_TRANSCODER_ENCODE_BIN,
                                                  RYGEL_GST_TRANSCODER_ENCODE_BIN,
                                                  &inner_error);
  if (inner_error) {
    g_propagate_error (error, inner_error);
    return NULL;
  }

  encoding_profile = rygel_gst_transcoder_get_encoding_profile (self);
  if (!encoding_profile) {
    g_set_error_literal (error,
                         RYGEL_GST_TRANSCODER_ERROR,
                         RYGEL_GST_TRANSCODER_ERROR_CANT_TRANSCODE,
                         "Could not create a transcoder configuration. "
                         "Your GStreamer installation might be missing a plug-in");
    return NULL;
  }

  g_object_set (self->priv->encoder, "profile", encoding_profile, NULL);
  gst_encoding_profile_unref (encoding_profile);
  g_debug ("%s using the following encoding profile:", G_OBJECT_TYPE_NAME (self));
  rygel_gst_utils_dump_encoding_profile (encoding_profile, 2);

  bin = GST_BIN (gst_bin_new ("transcoder-source"));
  gst_object_ref_sink (bin);

  /* Use the RygelGstDataSource-specific API to get the underlying Gstreamer element.
   */
  element = rygel_gst_data_source_get_gst_element (data_source);
  gst_bin_add_many (bin, element, priv->decoder, priv->encoder, NULL);

  gst_element_link (element, priv->decoder);

  g_signal_connect_object (priv->decoder,
                           "pad-added",
                           G_CALLBACK (on_decoder_pad_added),
                           self,
                           0);
  g_signal_connect_object (priv->decoder,
                           "autoplug_continue",
                           G_CALLBACK (on_decoder_autoplug_continue),
                           self,
                           0);
  g_signal_connect_object (priv->decoder,
                           "no-more-pads",
                           G_CALLBACK (on_decoder_no_more_pads),
                           self,
                           0);

  pad = gst_element_get_static_pad (priv->encoder, "src");
  ghost_pad = GST_GHOST_PAD (gst_ghost_pad_new (NULL, pad));
  gst_object_ref_sink (ghost_pad);
  gst_element_add_pad (GST_ELEMENT (bin), GST_PAD (ghost_pad));

  result = RYGEL_DATA_SOURCE (rygel_gst_data_source_new (GST_ELEMENT (bin)));
  gst_object_unref (ghost_pad);
  gst_object_unref (pad);
  gst_object_unref (bin);

  return result;
}

/**
 * Gets the GstEncodingProfile for this transcoder.
 *
 * @return the GstEncodingProfile for this transcoder. You must call gst_object_unref() on this.
 */
static GstEncodingProfile *
rygel_gst_transcoder_real_get_encoding_profile (RygelGstTranscoder *self) {
  /* This must be implemented by derived types. */
  g_critical ("Type `%s' does not implement abstract method `rygel_gst_transcoder_get_encoding_profile'", G_OBJECT_TYPE_NAME (self));
  return NULL;
}

GstEncodingProfile*
rygel_gst_transcoder_get_encoding_profile (RygelGstTranscoder *self) {
  g_return_val_if_fail (RYGEL_IS_GST_TRANSCODER (self), NULL);

  return RYGEL_GST_TRANSCODER_GET_CLASS (self)->get_encoding_profile (self);
}

const gchar*
rygel_gst_transcoder_get_preset (RygelGstTranscoder *self) {
  g_return_val_if_fail (RYGEL_IS_GST_TRANSCODER (self), NULL);

  return self->priv->preset;
}

void
rygel_gst_transcoder_set_preset (RygelGstTranscoder *self,
                                 const gchar *value) {
  RygelGstTranscoderPrivate *priv;

  g_return_if_fail (RYGEL_IS_GST_TRANSCODER (self));

  priv = self->priv;
  if (priv->preset) {
    g_free (priv->preset);
  }

  priv->preset = g_strdup (value);

  g_object_notify (G_OBJECT (self), "preset");
}

static
void rygel_gst_transcoder_dispose (GObject* object) {
  RygelGstTranscoder *self = RYGEL_GST_TRANSCODER (object);
  RygelGstTranscoderPrivate *priv = self->priv;

  if (priv->decoder) {
    GstElement *decoder = priv->decoder;

    priv->decoder = NULL;
    gst_object_unref (decoder);
  }
  if (priv->encoder) {
    GstElement *encoder = priv->encoder;

    priv->encoder = NULL;
    gst_object_unref (encoder);
  }

  G_OBJECT_CLASS (rygel_gst_transcoder_parent_class)->dispose (object);
}

static
void rygel_gst_transcoder_finalize (GObject* obj) {
  RygelGstTranscoder *self = RYGEL_GST_TRANSCODER (obj);

  g_free (self->priv->preset);

  G_OBJECT_CLASS (rygel_gst_transcoder_parent_class)->finalize (obj);
}

static void
rygel_gst_transcoder_get_property (GObject    *object,
				   guint       property_id,
				   GValue     *value,
				   GParamSpec *pspec) {
  RygelGstTranscoder *self = RYGEL_GST_TRANSCODER (object);

  switch (property_id) {
  case RYGEL_GST_TRANSCODER_PRESET:
    g_value_set_string (value, rygel_gst_transcoder_get_preset (self));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_gst_transcoder_set_property (GObject      *object,
				   guint         property_id,
				   const GValue *value,
				   GParamSpec   *pspec) {
  RygelGstTranscoder *self = RYGEL_GST_TRANSCODER (object);

  switch (property_id) {
  case RYGEL_GST_TRANSCODER_PRESET:
    rygel_gst_transcoder_set_preset (self, g_value_get_string (value));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static
void rygel_gst_transcoder_class_init (RygelGstTranscoderClass *gst_transcoder_class) {
  GObjectClass *object_class = G_OBJECT_CLASS (gst_transcoder_class);
  RygelTranscoderClass *transcoder_class = RYGEL_TRANSCODER_CLASS (gst_transcoder_class);

  transcoder_class->create_source = rygel_gst_transcoder_create_source;
  gst_transcoder_class->get_encoding_profile = rygel_gst_transcoder_real_get_encoding_profile;
  object_class->get_property = rygel_gst_transcoder_get_property;
  object_class->set_property = rygel_gst_transcoder_set_property;
  object_class->dispose = rygel_gst_transcoder_dispose;
  object_class->finalize = rygel_gst_transcoder_finalize;
  g_object_class_install_property (object_class,
                                   RYGEL_GST_TRANSCODER_PRESET,
                                   g_param_spec_string ("preset",
                                                        "preset",
                                                        "preset",
                                                        NULL,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));
  g_type_class_add_private (gst_transcoder_class, sizeof (RygelGstTranscoderPrivate));
}

static
void rygel_gst_transcoder_init (RygelGstTranscoder *self) {
  self->priv = RYGEL_GST_TRANSCODER_GET_PRIVATE (self);
  self->priv->preset = g_strdup (RYGEL_GST_TRANSCODER_DEFAULT_ENCODING_PRESET);
  self->priv->link_failed = TRUE;
}
