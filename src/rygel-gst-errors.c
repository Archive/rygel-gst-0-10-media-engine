/*
 * Copyright (C) 2013 Intel Corporation.
 *
 * Author: Krzesimir Nowak <krnowak@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-gst-errors.h"

GQuark
rygel_gst_error_quark (void) {
  return g_quark_from_static_string ("rygel-gst-error-quark");
}

GQuark
rygel_gst_data_source_error_quark (void) {
  return g_quark_from_static_string ("rygel-gst-data-source-error-quark");
}

GQuark
rygel_gst_transcoder_error_quark (void) {
  return g_quark_from_static_string ("rygel-gst-transcoder-error-quark");
}
