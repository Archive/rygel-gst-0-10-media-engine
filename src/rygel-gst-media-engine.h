/*
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __RYGEL_GST_0_10_MEDIA_ENGINE_MEDIA_ENGINE_H__
#define __RYGEL_GST_0_10_MEDIA_ENGINE_MEDIA_ENGINE_H__

#include <glib.h>
#include <glib-object.h>
#include <rygel-server.h>

G_BEGIN_DECLS

#define RYGEL_TYPE_GST_MEDIA_ENGINE (rygel_gst_media_engine_get_type ())
#define RYGEL_GST_MEDIA_ENGINE(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), RYGEL_TYPE_GST_MEDIA_ENGINE, RygelGstMediaEngine))
#define RYGEL_GST_MEDIA_ENGINE_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), RYGEL_TYPE_GST_MEDIA_ENGINE, RygelGstMediaEngineClass))
#define RYGEL_IS_GST_MEDIA_ENGINE(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), RYGEL_TYPE_GST_MEDIA_ENGINE))
#define RYGEL_IS_GST_MEDIA_ENGINE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), RYGEL_TYPE_GST_MEDIA_ENGINE))
#define RYGEL_GST_MEDIA_ENGINE_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), RYGEL_TYPE_GST_MEDIA_ENGINE, RygelGstMediaEngineClass))

typedef struct _RygelGstMediaEngine RygelGstMediaEngine;
typedef struct _RygelGstMediaEngineClass RygelGstMediaEngineClass;
typedef struct _RygelGstMediaEnginePrivate RygelGstMediaEnginePrivate;

struct _RygelGstMediaEngine {
  RygelMediaEngine parent_instance;
  RygelGstMediaEnginePrivate *priv;
};

struct _RygelGstMediaEngineClass {
  RygelMediaEngineClass parent_class;
};

GType
rygel_gst_media_engine_get_type (void) G_GNUC_CONST;

RygelGstMediaEngine *
rygel_gst_media_engine_new (void);

RygelMediaEngine *
module_get_instance (void);

G_END_DECLS

#endif /* __RYGEL_GST_0_10_MEDIA_ENGINE_MEDIA_ENGINE_H__ */
