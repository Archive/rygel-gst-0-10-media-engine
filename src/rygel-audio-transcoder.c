/*
 * Copyright (C) 2011 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Jens Georg <jensg@openismus.com>
 * Author: Murray Cumming <murrayc@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-audio-transcoder.h"
#include "rygel-gst-utils.h"

/**
 * Base class for all transcoders that handle audio.
 */

G_DEFINE_TYPE (RygelAudioTranscoder, rygel_audio_transcoder, RYGEL_TYPE_GST_TRANSCODER)

#define RYGEL_AUDIO_TRANSCODER_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), RYGEL_TYPE_AUDIO_TRANSCODER, RygelAudioTranscoderPrivate))

struct _RygelAudioTranscoderPrivate {
  gint audio_bitrate;
  GstCaps *container_format;
  GstCaps *audio_codec_format;
};

enum  {
  RYGEL_AUDIO_TRANSCODER_DUMMY_PROPERTY,
  RYGEL_AUDIO_TRANSCODER_AUDIO_BITRATE,
  RYGEL_AUDIO_TRANSCODER_AUDIO_CAPS,
  RYGEL_AUDIO_TRANSCODER_CONTAINER_CAPS
};

static void
rygel_audio_transcoder_dispose (GObject *object) {
  RygelAudioTranscoder *self = RYGEL_AUDIO_TRANSCODER (object);
  RygelAudioTranscoderPrivate *priv = self->priv;

  if (priv->container_format) {
    gst_caps_unref (priv->container_format);
  }

  if (priv->audio_codec_format) {
    gst_caps_unref (priv->audio_codec_format);
  }

  G_OBJECT_CLASS (rygel_audio_transcoder_parent_class)->dispose (object);
}

static void
rygel_audio_transcoder_get_property (GObject    *object,
                                     guint       property_id,
                                     GValue     *value,
                                     GParamSpec *pspec) {
  RygelAudioTranscoder *self = RYGEL_AUDIO_TRANSCODER (object);
  RygelAudioTranscoderPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_AUDIO_TRANSCODER_AUDIO_BITRATE:
    g_value_set_int (value, priv->audio_bitrate);
    break;

  case RYGEL_AUDIO_TRANSCODER_AUDIO_CAPS:
    g_value_take_string (value, gst_caps_to_string (priv->audio_codec_format));
    break;

  case RYGEL_AUDIO_TRANSCODER_CONTAINER_CAPS:
    g_value_take_string (value, gst_caps_to_string (priv->container_format));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_audio_transcoder_set_property (GObject      *object,
                                     guint         property_id,
                                     const GValue *value,
                                     GParamSpec   *pspec) {
  RygelAudioTranscoder *self = RYGEL_AUDIO_TRANSCODER (object);
  RygelAudioTranscoderPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_AUDIO_TRANSCODER_AUDIO_BITRATE:
    priv->audio_bitrate = g_value_get_int (value);
    break;

  case RYGEL_AUDIO_TRANSCODER_AUDIO_CAPS:
    if (priv->audio_codec_format) {
      gst_caps_unref (priv->audio_codec_format);
    }

    priv->audio_codec_format = rygel_gst_utils_caps_from_gvalue (value);
    break;

  case RYGEL_AUDIO_TRANSCODER_CONTAINER_CAPS:
    if (priv->container_format) {
      gst_caps_unref (priv->container_format);
    }

    priv->container_format = rygel_gst_utils_caps_from_gvalue (value);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static GUPnPDIDLLiteResource *
rygel_audio_transcoder_real_add_resource (RygelTranscoder        *base,
                                          GUPnPDIDLLiteItem      *didl_item,
                                          RygelMediaItem         *item,
                                          RygelTranscodeManager  *manager,
                                          GError                **error) {
  RygelAudioTranscoder *self = RYGEL_AUDIO_TRANSCODER (base);
  GError *inner_error = NULL;
  GUPnPDIDLLiteResource *resource = RYGEL_TRANSCODER_CLASS (rygel_audio_transcoder_parent_class)->add_resource (base, didl_item, item, manager, &inner_error);

  if (inner_error != NULL) {
    g_propagate_error (error, inner_error);
    return NULL;
  }

  if (resource == NULL) {
    return NULL;
  }

  gupnp_didl_lite_resource_set_bitrate (resource, (self->priv->audio_bitrate * 1000) / 8);
  return resource;
}

static guint
rygel_audio_transcoder_real_get_distance (RygelTranscoder *base,
                                          RygelMediaItem  *item) {
  RygelAudioTranscoder *self;
  RygelAudioItem *audio_item;
  guint distance;
  guint bitrate;

  if (!RYGEL_IS_AUDIO_ITEM (item) || RYGEL_IS_VIDEO_ITEM (item)) {
    return G_MAXUINT;
  }

  self = RYGEL_AUDIO_TRANSCODER (base);
  audio_item = RYGEL_AUDIO_ITEM (item);

  bitrate = rygel_audio_item_get_bitrate (audio_item);
  distance = 0;
  if (bitrate > 0) {
    distance = abs (bitrate - self->priv->audio_bitrate);
  }

  return distance;
}

static GstEncodingProfile *
rygel_audio_transcoder_real_get_encoding_profile (RygelGstTranscoder *base) {
  RygelAudioTranscoder *self = RYGEL_AUDIO_TRANSCODER (base);
  const gchar *preset = rygel_gst_transcoder_get_preset (RYGEL_GST_TRANSCODER (self));
  GstEncodingAudioProfile *enc_audio_profile = gst_encoding_audio_profile_new (self->priv->audio_codec_format, preset, NULL, 1);
  GstEncodingProfile *audio_profile = GST_ENCODING_PROFILE (enc_audio_profile);

  gst_encoding_profile_set_name (audio_profile, "audio");

  if (self->priv->container_format) {
    GstEncodingContainerProfile *enc_container_profile = gst_encoding_container_profile_new ("container", NULL, self->priv->container_format, preset);
    gst_encoding_container_profile_add_profile (enc_container_profile, audio_profile);
    return GST_ENCODING_PROFILE (enc_container_profile);
  }

  return audio_profile;
}

static void
rygel_audio_transcoder_class_init (RygelAudioTranscoderClass *rygel_audio_transcoder_class) {
  GObjectClass *object_class = G_OBJECT_CLASS (rygel_audio_transcoder_class);
  RygelTranscoderClass *transcoder_class = RYGEL_TRANSCODER_CLASS (rygel_audio_transcoder_class);
  RygelGstTranscoderClass *gst_transcoder_class = RYGEL_GST_TRANSCODER_CLASS (rygel_audio_transcoder_class);

  transcoder_class->add_resource = rygel_audio_transcoder_real_add_resource;
  transcoder_class->get_distance = rygel_audio_transcoder_real_get_distance;
  gst_transcoder_class->get_encoding_profile = rygel_audio_transcoder_real_get_encoding_profile;
  object_class->dispose = rygel_audio_transcoder_dispose;
  object_class->get_property = rygel_audio_transcoder_get_property;
  object_class->set_property = rygel_audio_transcoder_set_property;

  g_object_class_install_property (object_class,
                                   RYGEL_AUDIO_TRANSCODER_AUDIO_BITRATE,
                                   g_param_spec_int ("audio-bitrate",
                                                     "audio-bitrate",
                                                     "audio-bitrate",
                                                     0,
                                                     G_MAXINT,
                                                     0,
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB |
                                                     G_PARAM_CONSTRUCT_ONLY |
                                                     G_PARAM_READABLE |
                                                     G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   RYGEL_AUDIO_TRANSCODER_AUDIO_CAPS,
                                   g_param_spec_string ("audio-caps",
                                                        "audio-caps",
                                                        "audio-caps",
                                                        NULL,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_CONSTRUCT_ONLY |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));

  g_object_class_install_property (object_class,
                                   RYGEL_AUDIO_TRANSCODER_CONTAINER_CAPS,
                                   g_param_spec_string ("container-caps",
                                                        "container-caps",
                                                        "container-caps",
                                                        NULL,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_CONSTRUCT_ONLY |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));

  g_type_class_add_private (rygel_audio_transcoder_class,
                            sizeof (RygelAudioTranscoderPrivate));
}

static void
rygel_audio_transcoder_init (RygelAudioTranscoder *self) {
  self->priv = RYGEL_AUDIO_TRANSCODER_GET_PRIVATE (self);
  self->priv->audio_bitrate = 0;
  self->priv->container_format = NULL;
  self->priv->audio_codec_format = NULL;
}

gint
rygel_audio_transcoder_get_audio_bitrate (RygelAudioTranscoder *transcoder)
{
  g_return_val_if_fail (RYGEL_IS_AUDIO_TRANSCODER (transcoder), 0);

  return transcoder->priv->audio_bitrate;
}
