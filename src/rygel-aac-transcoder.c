/*
 * Copyright (C) 2011 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Luis de Bethencourt <luis.debethencourt@collabora.com>
 * Author: Murray Cumming <murrayc@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-aac-transcoder.h"

G_DEFINE_TYPE (RygelAACTranscoder, rygel_aac_transcoder, RYGEL_TYPE_AUDIO_TRANSCODER)

#define RYGEL_AAC_TRANSCODER_BITRATE 256
#define RYGEL_AAC_TRANSCODER_CODEC "audio/mpeg,mpegversion=4,stream-format=adts,rate=44100,base-profile=lc"

RygelAACTranscoder*
rygel_aac_transcoder_new (void) {
  return RYGEL_AAC_TRANSCODER (g_object_new (RYGEL_TYPE_AAC_TRANSCODER,
                                             "mime-type", "audio/vnd.dlna.adts",
                                             "dlna-profile", "AAC_ADTS_320",
                                             "extension", "adts",
                                             "preset", "Rygel AAC_ADTS_320 preset",
                                             "audio-bitrate", RYGEL_AAC_TRANSCODER_BITRATE,
                                             "audio-caps", RYGEL_AAC_TRANSCODER_CODEC,
                                             /* No "container-caps" here. */
                                             NULL));
}

static void
rygel_aac_transcoder_class_init (RygelAACTranscoderClass *aac_transcoder_class G_GNUC_UNUSED) {
}

static void
rygel_aac_transcoder_init (RygelAACTranscoder *self G_GNUC_UNUSED) {
}
