/*
 * Copyright (C) 2011 Nokia Corporation.
 * Copyright (C) 2012, 2013 Intel Corporation.
 *
 * Author: Jens Georg <jensg@openismus.com>
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-video-transcoder.h"
#include "rygel-gst-utils.h"

/**
 * Base class for all transcoders that handle video.
 */

G_DEFINE_TYPE (RygelVideoTranscoder,
               rygel_video_transcoder,
               RYGEL_TYPE_AUDIO_TRANSCODER)

struct _RygelVideoTranscoderPrivate {
  gint video_bitrate;
  GstCaps *video_codec_format;
  GstCaps *video_restrictions;
};

enum  {
  RYGEL_VIDEO_TRANSCODER_DUMMY_PROPERTY,
  RYGEL_VIDEO_TRANSCODER_VIDEO_BITRATE,
  RYGEL_VIDEO_TRANSCODER_VIDEO_CAPS,
  RYGEL_VIDEO_TRANSCODER_VIDEO_RESTRICTIONS
};

#define RYGEL_VIDEO_TRANSCODER_GET_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), \
                                RYGEL_TYPE_VIDEO_TRANSCODER, \
                                RygelVideoTranscoderPrivate))

static GUPnPDIDLLiteResource *
rygel_video_transcoder_real_add_resource (RygelTranscoder        *base,
                                          GUPnPDIDLLiteItem      *didl_item,
                                          RygelMediaItem         *item,
                                          RygelTranscodeManager  *manager,
                                          GError                **error) {
  RygelVideoTranscoder *self;
  RygelVisualItem *visual_item;
  GUPnPDIDLLiteResource *resource;
  GError *inner_error;
  gint audio_bitrate;

  g_return_val_if_fail (GUPNP_IS_DIDL_LITE_ITEM (didl_item), NULL);
  g_return_val_if_fail (RYGEL_IS_MEDIA_ITEM (item), NULL);
  g_return_val_if_fail (RYGEL_IS_TRANSCODE_MANAGER (manager), NULL);

  inner_error = NULL;

  resource = RYGEL_TRANSCODER_CLASS (rygel_video_transcoder_parent_class)->add_resource (base, didl_item, item, manager, &inner_error);
  if (inner_error != NULL) {
    g_propagate_error (error, inner_error);
    return NULL;
  }

  if (!resource) {
    return NULL;
  }

  self = RYGEL_VIDEO_TRANSCODER (base);
  audio_bitrate = rygel_audio_transcoder_get_audio_bitrate (RYGEL_AUDIO_TRANSCODER (self));

  visual_item = RYGEL_VISUAL_ITEM (item);
  gupnp_didl_lite_resource_set_width (resource,
    rygel_visual_item_get_width (RYGEL_VISUAL_ITEM (visual_item)));
  gupnp_didl_lite_resource_set_height (resource,
    rygel_visual_item_get_height (RYGEL_VISUAL_ITEM (visual_item)));
  gupnp_didl_lite_resource_set_bitrate (resource,
    (self->priv->video_bitrate + audio_bitrate) * 1000 / 8);

  return resource;
}

static guint
rygel_video_transcoder_real_get_distance (RygelTranscoder *base,
                                          RygelMediaItem  *item) {
  RygelVideoTranscoder *self;
  RygelAudioItem *audio_item;
  guint distance;
  guint bitrate;

  g_return_val_if_fail (RYGEL_IS_MEDIA_ITEM (item), G_MAXUINT);

  if (!RYGEL_IS_VIDEO_ITEM (item)) {
    return G_MAXUINT;
  }

  self = RYGEL_VIDEO_TRANSCODER (base);
  audio_item = RYGEL_AUDIO_ITEM (item);

  bitrate = rygel_audio_item_get_bitrate (audio_item);
  distance = 0;
  if (bitrate > 0) {
    distance += abs(bitrate - self->priv->video_bitrate);
  }

  return distance;
}

static GstEncodingProfile *
rygel_video_transcoder_real_get_encoding_profile (RygelGstTranscoder *base) {
  RygelVideoTranscoder *self = RYGEL_VIDEO_TRANSCODER (base);
  GstEncodingContainerProfile *enc_container_profile;
  GstEncodingProfile *enc_video_profile;

  enc_container_profile = GST_ENCODING_CONTAINER_PROFILE (RYGEL_GST_TRANSCODER_CLASS (rygel_video_transcoder_parent_class)->get_encoding_profile (RYGEL_GST_TRANSCODER (self)));
  enc_video_profile = GST_ENCODING_PROFILE (gst_encoding_video_profile_new (self->priv->video_codec_format,
                                                                            rygel_gst_transcoder_get_preset (RYGEL_GST_TRANSCODER (self)),
                                                                            self->priv->video_restrictions,
                                                                            1));

  gst_encoding_profile_set_name (enc_video_profile, "video");
  gst_encoding_container_profile_add_profile (enc_container_profile, enc_video_profile);

  return GST_ENCODING_PROFILE (enc_container_profile);
}

static void
rygel_video_transcoder_dispose (GObject *object) {
  RygelVideoTranscoder *self = RYGEL_VIDEO_TRANSCODER (object);
  RygelVideoTranscoderPrivate *priv = self->priv;

  if (priv->video_codec_format) {
    GstCaps *format = priv->video_codec_format;

    priv->video_codec_format = NULL;
    gst_caps_unref (format);
  }
  if (priv->video_restrictions) {
    GstCaps *restrictions = priv->video_restrictions;

    priv->video_restrictions = NULL;
    gst_caps_unref (restrictions);
  }

  G_OBJECT_CLASS (rygel_video_transcoder_parent_class)->dispose (object);
}

static void
rygel_video_transcoder_get_property (GObject    *object,
                                     guint       property_id,
                                     GValue     *value,
                                     GParamSpec *pspec) {
  RygelVideoTranscoder *self = RYGEL_VIDEO_TRANSCODER (object);
  RygelVideoTranscoderPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_VIDEO_TRANSCODER_VIDEO_BITRATE:
    g_value_set_int (value, priv->video_bitrate);
    break;

  case RYGEL_VIDEO_TRANSCODER_VIDEO_CAPS:
    g_value_take_string (value, gst_caps_to_string (priv->video_codec_format));
    break;

  case RYGEL_VIDEO_TRANSCODER_VIDEO_RESTRICTIONS:
    g_value_take_string (value, gst_caps_to_string (priv->video_restrictions));
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_video_transcoder_set_property (GObject      *object,
                                     guint         property_id,
                                     const GValue *value,
                                     GParamSpec   *pspec) {
  RygelVideoTranscoder *self = RYGEL_VIDEO_TRANSCODER (object);
  RygelVideoTranscoderPrivate *priv = self->priv;

  switch (property_id) {
  case RYGEL_VIDEO_TRANSCODER_VIDEO_BITRATE:
    priv->video_bitrate = g_value_get_int (value);
    break;

  case RYGEL_VIDEO_TRANSCODER_VIDEO_CAPS:
    if (priv->video_codec_format) {
      gst_caps_unref (priv->video_codec_format);
    }

    priv->video_codec_format = rygel_gst_utils_caps_from_gvalue (value);
    break;

  case RYGEL_VIDEO_TRANSCODER_VIDEO_RESTRICTIONS:
    if (priv->video_restrictions) {
      gst_caps_unref (priv->video_restrictions);
    }

    priv->video_restrictions = rygel_gst_utils_caps_from_gvalue (value);
    break;

  default:
    G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    break;
  }
}

static void
rygel_video_transcoder_class_init (RygelVideoTranscoderClass *video_transcoder_class) {
  GObjectClass *object_class = G_OBJECT_CLASS (video_transcoder_class);
  RygelTranscoderClass *transcoder_class = RYGEL_TRANSCODER_CLASS (video_transcoder_class);
  RygelGstTranscoderClass *gst_transcoder_class = RYGEL_GST_TRANSCODER_CLASS (video_transcoder_class);

  transcoder_class->add_resource = rygel_video_transcoder_real_add_resource;
  transcoder_class->get_distance = rygel_video_transcoder_real_get_distance;
  gst_transcoder_class->get_encoding_profile = rygel_video_transcoder_real_get_encoding_profile;
  object_class->dispose = rygel_video_transcoder_dispose;
  object_class->get_property = rygel_video_transcoder_get_property;
  object_class->set_property = rygel_video_transcoder_set_property;
  g_object_class_install_property (object_class,
                                   RYGEL_VIDEO_TRANSCODER_VIDEO_BITRATE,
                                   g_param_spec_int ("video-bitrate",
                                                     "video-bitrate",
                                                     "video-bitrate",
                                                     0,
                                                     G_MAXINT,
                                                     0,
                                                     G_PARAM_STATIC_NAME |
                                                     G_PARAM_STATIC_NICK |
                                                     G_PARAM_STATIC_BLURB |
                                                     G_PARAM_READABLE |
                                                     G_PARAM_WRITABLE));
  g_object_class_install_property (object_class,
                                   RYGEL_VIDEO_TRANSCODER_VIDEO_CAPS,
                                   g_param_spec_string ("video-caps",
                                                        "video-caps",
                                                        "video-caps",
                                                        NULL,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));
  g_object_class_install_property (object_class,
                                   RYGEL_VIDEO_TRANSCODER_VIDEO_RESTRICTIONS,
                                   g_param_spec_string ("video-restrictions",
                                                        "video-restrictions",
                                                        "video-restrictions",
                                                        NULL,
                                                        G_PARAM_STATIC_NAME |
                                                        G_PARAM_STATIC_NICK |
                                                        G_PARAM_STATIC_BLURB |
                                                        G_PARAM_READABLE |
                                                        G_PARAM_WRITABLE));
  g_type_class_add_private (object_class, sizeof (RygelVideoTranscoderPrivate));
}

static void
rygel_video_transcoder_init (RygelVideoTranscoder *self) {
  self->priv = RYGEL_VIDEO_TRANSCODER_GET_PRIVATE (self);
}
