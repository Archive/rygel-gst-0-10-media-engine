/*
 * Copyright (C) 2009 Nokia Corporation
 * Copyright (C) 2012, 2013 Intel Corporation
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "rygel-l16-transcoder.h"

G_DEFINE_TYPE (RygelL16Transcoder, rygel_l16_transcoder, RYGEL_TYPE_AUDIO_TRANSCODER)

#define RYGEL_L16_TRANSCODER_CHANNELS 2
#define RYGEL_L16_TRANSCODER_FREQUENCY 44100
#define RYGEL_L16_TRANSCODER_WIDTH 16
#define RYGEL_L16_TRANSCODER_DEPTH 16
#define RYGEL_L16_TRANSCODER_SIGNED true
#define RYGEL_L16_TRANSCODER_ENDIANNESS G_BIG_ENDIAN
#define RYGEL_L16_MIME_TYPE "audio/L" G_STRINGIFY (RYGEL_L16_TRANSCODER_WIDTH) \
                            ";rate=" G_STRINGIFY (RYGEL_L16_TRANSCODER_FREQUENCY) \
                            ";channels=" G_STRINGIFY (RYGEL_L16_TRANSCODER_CHANNELS)
#define RYGEL_L16_AUDIO_CAPS "audio/x-raw-int" \
                             ",channels=" G_STRINGIFY (RYGEL_L16_TRANSCODER_CHANNELS) \
                             ",rate=" G_STRINGIFY (RYGEL_L16_TRANSCODER_FREQUENCY) \
                             ",width=" G_STRINGIFY (RYGEL_L16_TRANSCODER_WIDTH) \
                             ",depth=" G_STRINGIFY (RYGEL_L16_TRANSCODER_DEPTH) \
                             ",signed=" G_STRINGIFY (RYGEL_L16_TRANSCODER_SIGNED) \
                             ",endianness=" G_STRINGIFY (RYGEL_L16_TRANSCODER_ENDIANNESS)


RygelL16Transcoder *
rygel_l16_transcoder_new (void) {
  return RYGEL_L16_TRANSCODER (g_object_new (RYGEL_TYPE_L16_TRANSCODER,
                                             "mime-type", RYGEL_L16_MIME_TYPE,
                                             "dlna-profile", "LPCM",
                                             "extension", "lpcm",
                                             /* No "preset". */
                                             /* No "audio-bitrate". */
                                             /* No "container-caps" */
                                             "audio-caps", RYGEL_L16_AUDIO_CAPS,
                                             NULL));
}

static GUPnPDIDLLiteResource *
rygel_l16_transcoder_real_add_resource (RygelTranscoder        *base,
                                        GUPnPDIDLLiteItem      *didl_item,
                                        RygelMediaItem         *item,
                                        RygelTranscodeManager  *manager,
                                        GError                **error) {
  GError *inner_error = NULL;
  GUPnPDIDLLiteResource* resource;
  gint bitrate;

  g_return_val_if_fail (GUPNP_IS_DIDL_LITE_ITEM (didl_item), NULL);
  g_return_val_if_fail (RYGEL_MEDIA_ITEM (item), NULL);
  g_return_val_if_fail (RYGEL_TRANSCODE_MANAGER (manager), NULL);

  resource = RYGEL_TRANSCODER_CLASS (rygel_l16_transcoder_parent_class)->add_resource (base, didl_item, item, manager, &inner_error);
  if (inner_error) {
    g_propagate_error (error, inner_error);
    return NULL;
  }

  if (!resource) {
    return NULL;
  }

  bitrate = ((RYGEL_L16_TRANSCODER_FREQUENCY * RYGEL_L16_TRANSCODER_CHANNELS) * RYGEL_L16_TRANSCODER_WIDTH) / 8;

  gupnp_didl_lite_resource_set_sample_freq (resource, RYGEL_L16_TRANSCODER_FREQUENCY);
  gupnp_didl_lite_resource_set_audio_channels (resource, RYGEL_L16_TRANSCODER_CHANNELS);
  gupnp_didl_lite_resource_set_bits_per_sample (resource, RYGEL_L16_TRANSCODER_WIDTH);
  gupnp_didl_lite_resource_set_bitrate (resource, bitrate);

  return resource;
}

static guint
rygel_l16_transcoder_real_get_distance (RygelTranscoder *base G_GNUC_UNUSED,
                                        RygelMediaItem  *item) {
  RygelAudioItem *audio_item;
  guint distance;
  guint sample_freq;
  guint channels;
  guint bits_per_sample;

  g_return_val_if_fail (RYGEL_IS_MEDIA_ITEM (item), G_MAXUINT);

  if (!RYGEL_IS_AUDIO_ITEM (item) || RYGEL_IS_VIDEO_ITEM (item)) {
    return G_MAXUINT;
  }

  audio_item = RYGEL_AUDIO_ITEM (item);
  distance = 0;

  sample_freq = rygel_audio_item_get_sample_freq (audio_item);
  if(sample_freq > 0) {
    distance += abs(sample_freq - RYGEL_L16_TRANSCODER_FREQUENCY);
  }

  channels = rygel_audio_item_get_channels (audio_item);
  if(channels > 0) {
    distance += abs(channels - RYGEL_L16_TRANSCODER_CHANNELS);
  }

  bits_per_sample = rygel_audio_item_get_bits_per_sample (audio_item);
  if(bits_per_sample > 0) {
    distance += abs(bits_per_sample - RYGEL_L16_TRANSCODER_WIDTH);
  }

  return distance;
}

static void
rygel_l16_transcoder_class_init (RygelL16TranscoderClass *l16_transcoder_class) {
  RygelTranscoderClass *transcoder_class = RYGEL_TRANSCODER_CLASS (l16_transcoder_class);
  transcoder_class->add_resource = rygel_l16_transcoder_real_add_resource;
  transcoder_class->get_distance = rygel_l16_transcoder_real_get_distance;
}

static void
rygel_l16_transcoder_init (RygelL16Transcoder *self G_GNUC_UNUSED) {
}
