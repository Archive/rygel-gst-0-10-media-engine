AC_PREREQ([2.64])
AC_INIT([rygel-gst-0-10-media-engine],
	[0.20.0],
	[http://bugzilla.gnome.org/enter_bug.cgi?product=Rygel],
	[rygel-gst-0-10-media-engine],
	[http://live.gnome.org/Rygel])
AC_CONFIG_AUX_DIR([build-aux])

AC_CONFIG_SRCDIR([src/rygel-gst-utils.c])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIR([m4])

AC_SEARCH_LIBS([strerror],[cposix])
AC_PROG_CC
AC_HEADER_STDC
AC_FUNC_MMAP

AC_SUBST(VERSION)

dnl Initialize automake
AM_INIT_AUTOMAKE([1.11.1 tar-ustar no-dist-gzip dist-xz -Wno-portability])
AM_MAINTAINER_MODE([enable])

dnl Initialize libtool
dnl Disable generation of static libraries
LT_PREREQ([2.2.6])
LT_INIT([dlopen disable-static])

dnl Required versions of library packages
LIBRYGEL_SERVER_REQUIRED=0.17.4
GUPNP_DLNA_REQUIRED=0.9.5
GSTREAMER_REQUIRED=0.10.36
GSTPBU_REQUIRED=0.10.35
REQUIRED_MODULES='gio-2.0 rygel-server-2.2 >= $LIBRYGEL_SERVER_REQUIRED gupnp-dlna-2.0 >= $GUPNP_DLNA_REQUIRED gstreamer-0.10 >= $GSTREAMER_REQUIRED gstreamer-base-0.10 >= $GSTREAMER_REQUIRED gstreamer-pbutils-0.10 >= $GSTPBU_REQUIRED'
PKG_CHECK_MODULES([DEPS], [$REQUIRED_MODULES])

dnl Debugging
AC_ARG_ENABLE(debug,
	AS_HELP_STRING([--enable-debug],[enable debugging]),,
        enable_debug=no)
if test "x$enable_debug" = "xyes"; then
        CFLAGS="$CFLAGS -O0 -g"
fi

dnl Strict compiler
AC_ARG_ENABLE(strict-cc,
	AS_HELP_STRING([--enable-strict-cc],[enable strict C compiler]),,
        enable_strict_cc=no)
if test "x$enable_strict_cc" = "xyes"; then
        CFLAGS="$CFLAGS -Wall -Werror"
fi


dnl Gettext
GETTEXT_PACKAGE=rygel-gst-0-10-media-engine
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, ["$GETTEXT_PACKAGE"],
  [Define the gettext package to be used])

AM_GLIB_GNU_GETTEXT
IT_PROG_INTLTOOL([0.40.0])

dnl Shave the output
AM_SILENT_RULES([yes])

PKG_PROG_PKG_CONFIG

RYGEL_PLUGIN_LINKER_FLAGS="`$PKG_CONFIG --variable=plugin_ldflags rygel-core-2.2`"
AC_SUBST(RYGEL_PLUGIN_LINKER_FLAGS)

if test "x$RYGEL_PLUGIN_LINKER_FLAGS" = 'x'
then
	AC_MSG_ERROR([Could not get Rygel plugin linker flags])
fi

RYGEL_ENGINE_DIR="`$PKG_CONFIG --variable=enginedir rygel-server-2.2`"
AC_SUBST(RYGEL_ENGINE_DIR)

RYGEL_VERSION="`$PKG_CONFIG --modversion rygel-server-2.2`"
AC_SUBST(RYGEL_VERSION)

if test "x$RYGEL_ENGINE_DIR" = 'x'
then
	AC_MSG_ERROR([Could not get Rygel media engine installation directory])
fi

AC_CONFIG_FILES([
Makefile
data/Makefile
src/Makefile
src/legacy-media-engine-gst.plugin
tests/Makefile
po/Makefile.in
])
AC_OUTPUT

echo "

                    ${PACKAGE} ${VERSION}
                    ============


        Prefix:                 ${prefix}
        Source code location:   ${srcdir}
        Compiler:               ${CC}
        CFLAGS:                 ${CFLAGS}
"
