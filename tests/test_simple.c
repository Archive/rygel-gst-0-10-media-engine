/*
 * Copyright (C) 2012 Intel Corporation
 *
 * This file is part of Rygel.
 *
 * Rygel is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Rygel is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include <gst/gst.h>
#include "rygel-gst-media-engine.h"

int main(int argc, char *argv[])
{
#if !GLIB_CHECK_VERSION(2,35,0)
  g_type_init ();
#endif
  gst_init (&argc, &argv);

  /* Some very simple checks that the media engine can be instantiated
   * and used in very simple ways.
   */
  RygelMediaEngine *media_engine = module_get_instance ();
  g_assert (media_engine);

  g_assert (RYGEL_GST_MEDIA_ENGINE (media_engine));

  RygelDataSource *data_source = NULL; /* TODO: rygel_media_engine_create_data_source (media_engine, NULL);
  g_assert (data_source); */

  GList* dlna_profiles = rygel_media_engine_get_dlna_profiles (media_engine);
  g_assert (dlna_profiles);

  GList* transcoders = rygel_media_engine_get_transcoders (media_engine);
  g_assert (transcoders);

  if (data_source)
    g_object_unref (data_source);
  g_object_unref (media_engine);

  return 0;
}
